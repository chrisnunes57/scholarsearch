.DEFAULT_GOAL := all

FILES := .gitignore                            \
    makefile                              \
    #.gitlab-ci.yml                        \

DOCKER_FILE_NAME= cyberguy99/scholarsearch:react

#.pylintrc:
#	pylint --disable=locally-disabled --reports=no --generate-rcfile > $@

all:

clean:
	rm -f  .coverage
	#rm -f  .pylintrc
	#rm -f  *.pyc
	rm -f  *.tmp
	#rm -rf __pycache__
	#rm -rf .mypy_cache

check: $(FILES)

config:
	git config -l

# should not to be called ever unless changing dockerfile
rebuild:
	docker-compose up --build --no-start

pull:
	docker-compose pull

push:
	docker-compose push

#format:
#	black Collatz.py
#	black RunCollatz.py
#	black TestCollatz.py

init:
	touch README
	git init
	git remote add origin git@gitlab.com:chrisnunes57/scholarsearch.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

react: 
	docker-compose run react sh

test-react:
    # npm install
	# docker-compose run react bash -c "CI=true npm run test -- --updateSnapshot"
	
test-postman:
	ls
	node_modules/.bin/newman run ss.postman_collection.json

flask:
	docker-compose run flask sh

run:
	docker-compose run

# run this inside docker container
frontendbuild:
	docker-compose run react npm run build

#deploy:
#	gsutil cp -r build gs://scholarsearch-static
#	gsutil cp app.yaml gs://scholarsearch-static

scrub:
	make clean
	rm -rf ./node_modules/
	rm -rf ./build/

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# TODO fix this
versions: 
	@echo
	which         vim
	vim           --version
