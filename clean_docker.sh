#!/bin/sh
docker system prune
docker volume ls -q -f 'dangling=true' | xargs docker volume rm
