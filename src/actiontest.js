import { places, meetups, events } from "../info/static";

const newCityInfo = places[0];
const newMeetupInfo = meetups[0];
const newEventInfo = events[0];

const CHANGE_SELECTED_EVENT = "CHANGE_SELECTED_EVENT";
const CHANGE_SELECTED_CITY = "CHANGE_SELECTED_CITY"
const CHANGE_SELECTED_COLLEGE = "CHANGE_SELECTED_COLLEGE"

function changeCityInfo(CityInfo) {
  return {
    type: CHANGE_SELECTED_CITY,
    CityInfo
  };
}

function changeEventInfo(EventInfo) {
  return {
    type: CHANGE_SELECTED_EVENT,
    EventInfo
  };
}

function changeEventInfo(CollegeInfo) {
  return {
    type: CHANGE_SELECTED_COLLEGE,
    CollegeInfo
  };
}

describe("Action Tests", () => {
  it("should equal the new CityInfo Action", () => {
    const CityInfo = newCityInfo;
    const expectedType = {
      type: CHANGE_SELECTED_CITY,
      CityInfo
    };
    expect(changeCityInfo(CityInfo)).toEqual(expectedType);
  });

  it("should equal the new MeetupInfo Action", () => {
    const MeetupInfo = newMeetupInfo;
    const expectedType = {
      type: CHANGE_SELECTED_MEETUP,
      MeetupInfo
    };
    expect(changeMeetupInfo(MeetupInfo)).toEqual(expectedType);
  });

  it("should equal the new EventInfo Action", () => {
    const EventInfo = newEventInfo;
    const expectedType = {
      type: CHANGE_SELECTED_EVENT,
      EventInfo
    };
    expect(changeEventInfo(EventInfo)).toEqual(expectedType);
  });
});
