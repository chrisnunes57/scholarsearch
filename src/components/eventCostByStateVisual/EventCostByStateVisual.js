import React from 'react';
import './EventCostByStateVisual.css';
// import {formatItem} from '../../utils';
import * as d3 from 'd3';

const QUERY = "https://api.scholarsearch.net/search/events" 

class EventCostByStateVisual extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      width: props.width,
      height: props.height
    }
    this.createVisual = this.createVisual.bind(this)
  }

  componentDidMount(){
    this.createVisual();
  }

  componentDidUpdate(){
    this.createVisual();
  }

  createVisual(){
  const node = this.node

    var eventCount = {};
    var costCount = {};

    //Get college data
    fetch(QUERY)
      .then((resp) => {
        return resp.json();
      }) // Transform the data into json
      .then((data) => {
        var addrRegEx = /(..)......$/;
        data.data.forEach((item) => {
          if(item.address != null && item.cost != null){
            var key = ("" + item.address).match(addrRegEx)[1];
            var cost = item.cost;
            console.log(key);
            if(! (key in eventCount)){
              eventCount[key] = 1;
            }
            else{
              eventCount[key] += 1;
            }
            if(! (key in costCount)){
              costCount[key] = cost;
            }
            else{
              costCount[key] += cost;
            }
          }
        });
      })
      .then(() => {
    var data = []
    var keys = []
    for(var key in eventCount){
      keys.push(key)
      data.push(costCount[key]/eventCount[key])
    }
    //Spacing variables
    var paddingLeft = 80;
    var paddingBottom = 80;
    var paddingTop = 100;

    const dataMax = d3.max(data);
    
    const xScale = d3.scaleBand()
      .domain(keys)
      .range([paddingLeft, this.state.width])
      .padding(0.1);
    
    const yScale = d3.scaleLinear()
      .domain([0, dataMax])
      .range([this.state.height - paddingBottom, paddingTop]);
    
    const yAxis = d3.axisLeft(yScale)

    const xAxis = d3.axisBottom(xScale);

    //Add y axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'yAxis')
      .attr('transform', 'translate(' + paddingLeft + ',0)')
      .call(yAxis);

    //Add x axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'xAxis')
      .attr('transform', 'translate(0, ' + (this.state.height - paddingBottom) + ')')
      .call(xAxis);

    d3.select(node)
      .append('text')
      .attr('x', 30)
      .attr('y', yScale(dataMax / 2))
      .attr('text-anchor', 'middle')
      .attr('transform', 'rotate(270, 30,' + yScale(dataMax/2) + ')')
      .text('Average Cost ($)');
    
    d3.select(node)
      .append('text')
      .attr('x', (this.state.width - paddingLeft) / 2)
      .attr('y', (this.state.height - 30))
      .attr('text-anchor', 'middle')
      .text('State');
    
    //Link the data to g tags and add a child rect
    d3.select(node)
      .selectAll('#bar')
      .data(data)
      .enter()
      .append('g')
      .attr('id', 'bar')
      .append('rect')
      .style('fill', '#007bff')
      .attr('x', (d,i) => xScale(keys[i]))
      .attr('y', d => yScale(d))
      .attr('height', d => this.state.height - yScale(d) - paddingBottom)
      .attr('width', xScale.bandwidth())
      .attr('stroke-width', 3)
      .attr('stroke', '#000000');
   
    //Add a child text tag with the number of colleges
    d3.select(node)
      .selectAll('#bar')
      .append('text')
      .attr('x', (d,i) => xScale(keys[i]) + xScale.bandwidth() * 0.5)
      .attr('y', d => yScale(d) - 30)
      .attr('text-anchor', 'middle')
      .text(d => "" + d.toFixed(2));

      });
  }

  render() {
    return(
      <svg ref={node => this.node = node}
      width={this.state.width} height={this.state.height}>
      </svg>
    );
  }
}

export default EventCostByStateVisual;
