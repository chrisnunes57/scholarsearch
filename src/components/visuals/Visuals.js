import React from 'react';
import './Visuals.css';
import CollegesByMajorVisual from '../collegesByMajorVisual/CollegesByMajorVisual';
import EventCostByStateVisual from '../eventCostByStateVisual/EventCostByStateVisual';
import LocationMusicEventVisual from '../locationMusicEventVisual/LocationMusicEventVisual';
import PopulationMeetupVisual from '../populationMeetupVisual/PopulationMeetupVisual';
import MusicHeatMap from '../musicHeatMap/MusicHeatMap';
import MusicTimeline from '../musicTimeline/MusicTimeline';

class Visuals extends React.Component{
   constructor(props) {
     super(props);
   }
  
   componentDidMount(){
   
   }
  
   componentDidUpdate(){
  
   }

  render(){
    return(
      <section className="container-fluid">
        <div className="row">
          <div className="col-12">
            <h4>Number of Colleges with a Certain Popular Major</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <CollegesByMajorVisual width="1140" height="700"/>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h4>Average Cost of Events by State</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <EventCostByStateVisual width="1140" height="700"/>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h3>AfterWork Visuals</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h4>Location vs. Number of Events</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <MusicHeatMap width="1140" height="800"/>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h4>Population vs. Number of Events</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <PopulationMeetupVisual width="1140" height="500"/>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h4>Music Genre Map</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h4>Music Event Count vs. Month</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <MusicTimeline width="1140" height="500"/>
          </div>
        </div>
      </section>
    );
  }
}

export default Visuals;
