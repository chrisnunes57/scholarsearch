import React from 'react';
import './FilterList.css';
import Filter from "./Filter";

class FilterList extends React.Component {

  render() {
    return (
        this.props.filters.map((item, index) => {
          return <Filter updateFilters={this.props.updateFilters} key={index} filterObject={item} items={this.props.items}/>
        })
    );
  }
}

export default FilterList;
