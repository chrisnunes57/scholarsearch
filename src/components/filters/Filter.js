import React from 'react';
import './FilterList.css';

class Filter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      item: props.filterObject,
      items: props.items
    };

      this.updateFilters = props.updateFilters;
      this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    let propValues = this.state.item.options;
    if(propValues.length === 0) {
      this.state.items.forEach(item => {
          let val = item["importantProperties"][this.state.item.key] || item["additionalProperties"][this.state.item.key];
          if(!propValues.includes(val)) {
            propValues.push(val)
          }
      });
    }

    this.setState({propValues: propValues});
  }

  handleChange(e) {
      // strip value of extra characters
      let val = e.target.value.replace(/[$,%]+/g,"");
      this.updateFilters(this.props.filterObject.key, this.props.filterObject.url_key, val, this.props.filterObject.type || "")
  }

  render() {
    // TODO: Make these <select> elements state controlled so that values carry over
    return (
        <div className="form-group col-md">
          <label htmlFor="inputState">{this.props.filterObject.key}</label>
          <select id="inputState" className="form-control" onChange={this.handleChange}>
            <option defaultValue>Choose...</option>
            {this.state.propValues && this.state.propValues.map((item, index) => {
              return <option key={index}>{item}</option>
            })}
          </select>
        </div>
    );
  }
}

export default Filter;
