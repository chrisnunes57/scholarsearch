import React from 'react';
import {formatItem} from '../../utils';
import './LocationMusicEventVisual.css';
import * as d3 from 'd3';

const QUERY = "https://api.after-work.site/events"

class LocationMusicEventVisual extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      width: props.width || null,
      height: props.height || null
    };
    this.updateVisual = this.updateVisual.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.updateVisual();
  }

  componentDidUpdate(){
    this.updateVisual();
  }

  getData(){
    
  }

  updateVisual(){
    const node = this.node

    var locationEventCount = {};

    //Get college data
    fetch(QUERY)
      .then((resp) => {
        return resp.json();
      }) // Transform the data into json
      .then((data) => {
        data.forEach((item) => {
          if(item.category === 'Music'){
            var key = "" + item.city_name; 
            if(! (key in locationEventCount)){            
              locationEventCount[key] = {
                name: key,
                value: 1
              };
            }
            else{
              locationEventCount[key].value += 1; 
            }
          }
        })
        
        //var locationEventCountArray = [];
        //for(var key in locationEventCount){
        //  locationEventCountArray.push(locationEventCount[key]);
        //}
      })
      .then(() => {
    var data = [];
    var keys = [];
    for(var key in locationEventCount){
      keys.push(locationEventCount[key].name);
      data.push(locationEventCount[key].value);
    }

    //Spacing variables
    var paddingLeft = 30;
    var paddingBottom = 100;
    var paddingTop = 100;

    const dataMax = d3.max(data);
    const xScale = d3.scaleBand()
      .domain(keys)
      .range([paddingLeft, this.state.width])
      .padding(0.1);
    
    const yScale = d3.scaleLinear()
      .domain([0, dataMax])
      .range([this.state.height - paddingBottom, paddingTop]);
    
    const yAxis = d3.axisLeft(yScale)

    const xAxis = d3.axisBottom(xScale);

    //Add y axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'yAxis')
      .attr('transform', 'translate(30,0)')
      .call(yAxis);

    //Add x axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'xAxis')
      .attr('transform', 'translate(0, ' + (this.state.height - paddingBottom) + ')')
      .call(xAxis);

    d3.select(node)
      .selectAll('#xAxis')
      .selectAll('.tick')
      .selectAll('text')
      .attr('dx', -40)
      .attr('dy', -5)
      .attr('transform', 'rotate(270)')
    
    //Link the data to g tags and add a child rect
    d3.select(node)
      .selectAll('#bar')
      .data(data)
      .enter()
      .append('g')
      .attr('id', 'bar')
      .append('rect')
      .style('fill', '#007bff')
      .attr('x', (d,i) => xScale(keys[i]))
      .attr('y', d => yScale(d))
      .attr('height', d => this.state.height - yScale(d) - paddingBottom)
      .attr('width', xScale.bandwidth())
      .attr('stroke-width', 3)
      .attr('stroke', '#000000');
   
    //Add a child text tag with the number of colleges
    d3.select(node)
      .selectAll('#bar')
      .append('text')
      .attr('x', (d,i) => xScale(keys[i]) + xScale.bandwidth() * 0.5)
      .attr('y', d => yScale(d) - 30)
      .attr('text-anchor', 'middle')
      .text(d => "" + d);
    });
  }

  render() {
    return(
      <svg ref={node => this.node = node}
      width={this.state.width} height={this.state.height}>
      </svg>
    );
  }

}

export default LocationMusicEventVisual;
