import React from 'react';
import './Pagination.css';
import { Link } from "react-router-dom";

class Pagination extends React.Component {

  parseQuery(query, page) {
    var regex = /page=[0-9]*/;
    console.log(query);
    if (query === "") {
      return window.location.pathname + "?page=" + page;
    }
    if (query.includes("page=")) {
      return query.replace(regex, 'page=' + page);
    }
    return query + "&page=" + page;
    
  }


  constructor(props) {
    super(props);
    this.state = {
      pageNumber: this.props.pageNumber,
      query: this.props.query,
      nextPage: this.props.nextPage
    }
    this.parseQuery(this.state.query);

  }

  componentDidUpdate(prev) {
    if(prev.pageNumber !== this.props.pageNumber) {
      this.setState({
        pageNumber: this.props.pageNumber,
        query: this.props.query,
        nextPage: this.state.nextPage
      });
    }
    this.parseQuery(this.state.query);
  }

  render() {
    return (
      <div className="pagination-wrapper">
        {this.state.pageNumber > 1 ? <Link to={this.parseQuery(this.state.query, parseInt(this.state.pageNumber, 10) - 1)} className="pagination-link">Prev</Link> : <Link to="/" className="pagination-link" />}
        <p style={{display: "inline-block"}}>{this.state.pageNumber}</p>
        {this.state.nextPage && <Link to={this.parseQuery(this.state.query, parseInt(this.state.pageNumber, 10) + 1)} className="pagination-link">Next</Link>}
      </div>
    );
  }
}

export default Pagination;
