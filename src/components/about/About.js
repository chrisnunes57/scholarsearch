import React, {Fragment} from 'react';
import './About.css';
import ItemList from '../itemlist/ItemList';
import TechUsed from '../item/TechUsed';

import { Col, Row } from "react-bootstrap";

import aws_certificate_manager from "../tools/aws_certificate_manager.png";
import bootstrap from "../tools/bootstrap.jpg";
import docker from "../tools/docker.png";
import docker_compose from "../tools/docker_compose.png";
import flask from "../tools/flask.png";
import flask_marshmallow from "../tools/flask_marshmallow.jpeg";
import flask_sqlalchemy from "../tools/flask_sqlalchemy.png";
import gcp from "../tools/gcp.jpg";
// import git from "../tools/git.jpg";
import jest from "../tools/jest.png";
import mysql from "../tools/mysql.png";
import postman from "../tools/postman.png";
import react from "../tools/react.png";
import react_router from "../tools/react_router.png";
import route53 from "../tools/route53.png";
import selenium from "../tools/selenium.jpg";
import plantuml from "../tools/plantuml.png";

import census from "../tools/census.png";
import collegescorecard from "../tools/collegescorecard.png";
import openweather from "../tools/openweather.png";
import ticketmaster from "../tools/ticketmaster.png";
import opendatasoft from "../tools/opendatasoft.png";
import numbeo from "../tools/numbeo.jpg";
import googlecustomsearch from "../tools/googlecustomsearch.jpg";


const projectAPI = 'https://gitlab.com/api/v4/projects/14586333/repository/contributors';
const issuesAPI = 'https://gitlab.com/api/v4/projects/14586333/issues_statistics?author_username=';
const avatarAPI = 'https://gitlab.com/api/v4/avatar?email=';

class About extends React.Component {

// This is the API endpoint for our about page
//   https://gitlab.com/api/v4/projects/14586333/repository/contributors

  constructor(props) {
    super(props);
    this.state = {
      team: [
        {
          imgSrc: null,
          name: "Chris Nunes",
          username: "chrisnunes57",
          email: "chrisnunes57@gmail.com",
          bio: "The Javascript Guy",
          role: "Frontend Wizard",
          commits: 0,
          issues: 0,
          unittests: 13
        },
        {
          imgSrc: null,
          name: "Gautham Gujjula",
          username: "ggujjula",
          email: "gauthamgujjula@utexas.edu",
          bio: "2nd year CS student, hoping to get into the systems or security space. Hobbies include hiking and binge watching Netflix.",
          role: "Backend Wizard",
          commits: 0,
          issues: 0,
          unittests: 25
        },
        {
          imgSrc: null,
          name: "Hadi Channaa",
          username: "Hadi_channaa",
          email: "aahdi600@gmail.com",
          bio: "3rd Year UTCS student that is interested in memes. Hobbies include Playing video games, playing the saxophone, anime, Pokemon, and costume design",
          role: "Frontend Wizard",
          commits: 0,
          issues: 0,
          unittests: 17
        },
        {
          imgSrc: null,
          name: "Rishi Iyer",
          username: "rvi857",
          email: "iyer.rishi@gmail.com",
          bio: "Rishi Iyer was born and raised in the Bay Area, California. He enjoys psychology, cooking, and dance. For this project, Rishi was in charge of scraping data using the python requests package and setting up the database along with Rushil.",
          role: "Backend Wizard",
          commits: 0,
          issues: 0,
          unittests: 0
        },
        {
          imgSrc: null,
          name: "Rushil Dandamudi",
          username: "CyberGuy99",
          email: "rushilcd@gmail.com",
          bio: "3rd Year UTCS student interested in quantum information research. Hobbies include board games, anime, Pokemon, Taekwondo and hiking",
          role: "Backend Wizard",
          commits: 0,
          issues: 0,
          unittests: 0
        }
      ],
      totalcommits: 0,
      totalissues: 0,
      totalunittest: 30
      
    };
    this.contributors();
  }


  checkForImages(data) {
    this.setState({ team: data });
  }


  updateTeam(newMember) {
    let found = null;
    this.state.team.forEach((member) => {
      if(newMember.name === member.name) {
        found = member;
        member.commits += newMember.commits;
        this.setState({totalcommits : this.state.totalcommits + newMember.commits});
      }
    });
    if(!newMember.email.includes("@cs")) {
        found.imgSrc = newMember.imgSrc;
      }
    this.setState({loaded: true});
  } 

  renderUsers(data) {
    data.forEach((item) => {
      fetch(avatarAPI + item.email + "&size=225")
        .then(response => response.json())
        .then((userAvatar) => {
          item.imgSrc = userAvatar.avatar_url;
      this.updateTeam(item);
    }); 
    });
  }


  contributors() {
    fetch(projectAPI)
      .then(response => response.json())
      .then(data => this.renderUsers(data));
      this.state.team.forEach((item) => {
        fetch(issuesAPI + item.username)
        .then(response => response.json())
        .then((issueInfo) => {
          item.issues = issueInfo.statistics.counts.all;
          this.setState({totalissues : this.state.totalissues + issueInfo.statistics.counts.all});
        });
      });
  }

  displayTech() {   
    return (
      <div className="container tech-stack">
        <Row>
          <Col>
            <TechUsed
              image={aws_certificate_manager}
              name="AWS certificate manager"
              description="Allows SSL/TLS certification of domian. Enables HTTPS"
            />
          </Col>
          <Col>
            <TechUsed
              image={bootstrap}
              name="Bootstrap"
              description="CSS framework"
            />
          </Col>
          <Col>
            <TechUsed
              image={docker}
              name="docker"
              description="Provides OS level virtualization"
            />
          </Col>
          <Col>
            <TechUsed
              image={docker_compose}
              name="docker compose"
              description="supplement to docker for containerizing projects"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <TechUsed
              image={flask}
              name="flask"
              description="Python framework"
            />
          </Col>
          <Col>
            <TechUsed
              image={flask_marshmallow}
              name="flask marshmallow"
              description="Object serialization"
            />
          </Col>
          <Col>
            <TechUsed
              image={flask_sqlalchemy}
              name="flask sqlalchemy"
              description="SQL database support for flask"
            />
          </Col>
          <Col>
            <TechUsed
              image={gcp}
              name="gcp"
              description="Server and frontend hosting"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <TechUsed
                image={plantuml}
                name="PlantUML"
                description="Creates a visual UML Diagram"
            />
          </Col>
          <Col>
            <TechUsed
              image={jest}
              name="jest"
              description="javascript testing"
            />
          </Col>
          <Col>
            <TechUsed
              image={mysql}
              name="mysql"
              description="SQL database engine"
            />
          </Col>
          <Col>
            <TechUsed
              image={postman}
              name="postman"
              description="API testing"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <TechUsed
              image={react}
              name="react"
              description="Frontend javascript framework"
            />
          </Col>
          <Col>
            <TechUsed
              image={react_router}
              name="react router"
              description="Routing library for react"
            />
          </Col>
          <Col>
            <TechUsed
              image={route53}
              name="route53"
              description="Domain name registration"
            />
          </Col>
          <Col>
            <TechUsed
              image={selenium}
              name="selenium"
              description="Web app testing"
            />
          </Col>
        </Row>
      </div>
    );
  }

  displayAPI() {   
    return (
      <div className="container tech-stack">
        <Row>
          <Col>
            <TechUsed
              image={openweather}
              name="Open Weather"
              description=""
            />
          </Col>
          <Col>
            <TechUsed
              image={collegescorecard}
              name="College Scorecard"
              description=""
            />
          </Col>
          <Col>
            <TechUsed
              image={ticketmaster}
              name="Ticket master"
              description=""
            />
          </Col>
          <Col>
            <TechUsed
              image={numbeo}
              name="Numbeo"
              description=""
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <TechUsed
              image={census}
              name="Census.gov"
              description=""
            />
          </Col>
          <Col>
            <TechUsed
              image={opendatasoft}
              name="Open DataSoft"
              description=""
            />
          </Col>
          <Col>
            <TechUsed
              image={googlecustomsearch}
              name="Google Custom Search Image"
              description=""
            />
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    if(this.state.team) {
      return (
        <Fragment>
          <h1 style={{marginBottom: "20px"}}>Our Purpose</h1>
          <p style={{marginBottom: "60px", marginLeft: "300px", marginRight: "300px"}}>The goal of our website is to have a place students can go to find their ideal college. The website allows a student to view a list of all colleges in the United States, as well as the cities those colleges are located in and the events that are happening in that city. This allows the student to easily view the kind of academics, social setting, and cost of living they can expect if they attend college and/or live in a certain place. We hope that the ability to easily view and search through this data will make it easier for the student to make a decision when admission season rolls around.</ p> 

          <h1 style={{marginBottom: "60px"}}>Meet the Team</h1>
          <ItemList items={this.state.team.length > 0 ? this.state.team : null} type="team"/>

          <h4 style={{marginBottom: "20px"}}>Total Commits: {this.state.totalcommits}</h4>
          <h4 style={{marginBottom: "20px"}}>Total Issues: {this.state.totalissues}</h4>
          <h4 style={{marginBottom: "20px"}}>Total Unit Tests: {this.state.totalunittest}</h4>
          <h1>Technologes Used</h1>
            {this.displayTech()}
          {this.state.team.length > 0 && <a href="https://documenter.getpostman.com/view/9278671/SW7ey5YD"><h1 style={{marginBottom: "20px"}}>API Documentation</h1></a> }
          {<a href="https://gitlab.com/chrisnunes57/scholarsearch"><h1 style={{marginBottom: "60px"}}>GitLab Respository</h1></a> }
          <h1 style={{marginBottom: "20px"}}>3rd Party API's for Data</h1>
            {this.displayAPI()}
        </ Fragment>
      );
    } else {
      return <h1>Loading</h1>
    }
  }
}

export default About;
