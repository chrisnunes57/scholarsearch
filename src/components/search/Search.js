import React, {Fragment} from 'react';
import qs from "query-string";
import {formatItem} from "../../utils";
import ItemList from "../itemlist/ItemList";
import equal from "fast-deep-equal";

const ENDPOINT = "https://api.scholarsearch.net/search/";

class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      model: "colleges",
      items: [],
      pageNumber: qs.parse(this.props.location.search).page || 1,
      filters: qs.parse(this.props.location.search).filters,
      mins: qs.parse(this.props.location.search).mins,
      maxes: qs.parse(this.props.location.search).maxes,
      f2: qs.parse(this.props.location.search).f2
    }
  }

  makeApiCall(pageNumber, searchToken, filters, mins, maxes, f2) {
    fetch(`${ENDPOINT}${this.state.model}?${pageNumber ? "&page=" + pageNumber : ""}${searchToken ? "&token=" + searchToken : ""}${filters ? "&filters=" + filters : ""}${mins ? "&mins=" + mins : ""}${maxes ? "&maxes=" + maxes : ""}${f2 ? "&f2=" + f2 : ""}`)
        .then((resp) => {
          console.log(resp)
          return resp.json();
        }) // Transform the data into json
        .then((data) => {
          if(data.data.length > 0) {
            console.log(data)
            // Process data
            let extras = data.extras[0];

            let ids = [];
            Object.keys(data.extras[0]).forEach(key => {
              ids.push(key)
            });

            data.data.forEach((item) => {

              let matches = data.extras[0][item.id] || ""
              formatItem(item, this.state.model, matches);

              item.additionalProperties = {
                "Age Restriction": item.event || "None"
              };

              item.matchText = extras && extras[item.id];
              item.matchQuery = searchToken;

              this.state.items.push(item);
            });
          } else {
            console.log("no data")
            this.setState({items: []})
          }
        }).then(() => {
          this.setState({loaded: true});
        });
  }

  componentDidMount() {
    this.makeApiCall(this.state.pageNumber, this.state.searchToken, this.state.filters, this.state.mins, this.state.maxes, this.state.f2);
  }

  componentDidUpdate(prev) {
    if(!equal(prev.location, this.props.location)) {
      let temp = qs.parse(this.props.location.search).page || 1;
      let temp2 = qs.parse(this.props.location.search).token;
      let temp3 = qs.parse(this.props.location.search).filters;
      let temp4 = qs.parse(this.props.location.search).mins;
      let temp5 = qs.parse(this.props.location.search).maxes;
      let temp6 = qs.parse(this.props.location.search).f2;
      this.setState({
        pageNumber: temp,
        searchToken : temp2,
        filters: temp3,
        mins: temp4,
        maxes: temp5,
        f2: temp6,
        items: []
      }, () => {
        this.makeApiCall(temp, temp2, temp3, temp4, temp5, temp6);
      });
    }
  }

  handleClick(e) {
    let newModel = e.target.innerText.toLocaleLowerCase();
    if(newModel !== this.state.model) {
      this.setState({model: newModel, loaded: false, items: []}, () => {
        // After we update the model in state, we update list of items
        this.makeApiCall(this.state.pageNumber, this.state.searchToken, this.state.filters, this.state.mins, this.state.maxes, this.state.f2);
      });
    }
  }

  render() {
    return (
        <Fragment>
          <section className="container">
            <div className="row">
              <div className="col-md-4">
                <button onClick={(e) => this.handleClick(e)}>Colleges</button>
              </div>
              <div className="col-md-4">
                <button onClick={(e) => this.handleClick(e)}>Events</button>
              </div>
              <div className="col-md-4">
                <button onClick={(e) => this.handleClick(e)}>Cities</button>
              </div>
            </div>
          </section>
          <ItemList items={this.state.items.length > 0 ? this.state.items : (this.state.loaded ? [] : null)} type="grid" global={true}
                    modelType={"search/" + this.state.model}  pageNumber={this.state.pageNumber} query={qs.parse(this.props.location.search).query}/>
        </Fragment>

    )
  }
}

export default Search;
