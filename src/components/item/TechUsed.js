
import React from "react";
import { Image, Card } from "react-bootstrap";
//import "../css_files/App.css";
import "./TechUsed.css";

export class TechUsed extends React.Component {

  render() {
    return (
      <Card>
        <Image className="image-center" src={this.props.image} style={{ width: "10rem" }} />
        <div className="card-text">
          <h1>{this.props.name}</h1>
          <p>{this.props.description}</p>
        </div>
      </Card>
    );
  }
}

export default TechUsed;
