import React, {Fragment} from 'react';
import './Item.css';

class AdditionalProperties extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      additionalProperties: []
    }
  }

  componentDidMount() {
    Object.keys(this.props.item.additionalProperties).forEach((key,index) => {
      // key: the name of the object key
      // index: the ordinal position of the key within the object
      if(key !== "type" && key !== "name" && key !== "state" && key !== "city") {
        this.state.additionalProperties.push(key);
      }
    });
    this.setState({loaded: true})
  }

  render() {

    return (
      <Fragment>
        {this.state.additionalProperties.map((key, index) => {
          return <p className={this.props.cName} style={{display: "inline-block", margin: "0 30px"}} key={index}>{key}: <b>{this.props.item.additionalProperties[key]}</b></p>
        })}
      </Fragment>
    );
  }
}

export default AdditionalProperties;
