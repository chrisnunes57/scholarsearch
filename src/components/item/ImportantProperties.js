import React, {Fragment} from 'react';
import './Item.css';

class ImportantProperties extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      importantProperties: []
    }
  }

  componentDidMount() {
    Object.keys(this.props.item.importantProperties).forEach((key,index) => {
      // key: the name of the object key
      // index: the ordinal position of the key within the object
      if(key !== "type" && key !== "name" && key !== "state" && key !== "city") {
        this.state.importantProperties.push(key);
      }
    });
    this.setState({loaded: true})
  }

  render() {

    return (
      <Fragment>
        {this.state.importantProperties.map((key, index) => {
          return <p className={this.props.cName} key={index}>{key}: <b>{this.props.item.importantProperties[key]}</b></p>
        })}
      </Fragment>
    );
  }
}

export default ImportantProperties;
