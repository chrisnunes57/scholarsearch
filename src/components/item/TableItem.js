import React from 'react';
import './Item.css';
import { Link } from "react-router-dom";
import {getLocation, getSlug, setCacheItem} from '../../utils';

class Item extends React.Component {

  render() {

    return (
      <div className="item-row">
        <Link onClick={() => setCacheItem(getSlug(this.props.item.name), this.props.item)} to={{
          pathname: "/" + this.props.item.type + "/" + getSlug(this.props.item.name),
          item: this.props.item
        }}>
          <h5>{this.props.item.name || ""}</h5>
        </ Link>
        <p>{getLocation(this.props.item)}</p>
      </div>
    );
  }
}

export default Item;
