import React, {Fragment} from 'react';
import './MajorPercentagesVisual.css';
import * as d3 from 'd3';

class MajorPercentagesVisual extends React.Component{
	constructor(props) {
    super(props);
    this.state = {
      width: props.width || null,
      height: props.height || null,
      majorData: props.majorData || null
    }
    this.createVisual = this.createVisual.bind(this)
  }

  componentDidMount(){
    this.createVisual();
  }

  componentDidUpdate(){
    this.createVisual();
  }

  createVisual(){
    //consolidate costs by category
    var categoryMajorDataObj = {};
    var totalCost = [0];
    this.state.majorData.forEach((datum) => {
	    categoryMajorDataObj[datum.name] = {
	      name: datum.name,
	      value: datum.percent_of_undergrad
	    };
    });

    var categoryMajorDataArray = [];
    for(var datum in categoryMajorDataObj){
      categoryMajorDataArray.push(categoryMajorDataObj[datum]);
    }

    const node = this.node;

    const pieGen = d3.pie()
      .sort((i, j) => i.value < j.value)
      .value((d) => d.value);

    var arcs = pieGen(categoryMajorDataArray);

    const arcGen = d3.arc()
      .innerRadius(0)
      .outerRadius(250);

    const labelScale = d3.scaleLinear()
      .domain([0, categoryMajorDataArray.length])
      .range([60, this.state.height - 30]);

    d3.select(node)
      .selectAll('#total')
      .data(totalCost)
      .enter()
      .append('g')
      .attr('id', 'total')
      .append('text')
      .attr("x", 250)
      .attr("y", 560)
      .attr('text-anchor', 'middle')
      .attr('font-weight', 'bold')

    d3.select(node)
      .selectAll('#datum')
      .data(arcs)
      .enter()
      .append('g')
      .attr('id', 'datum');

    d3.select(node)
      .selectAll('#datum')
      .append('path')
      .attr("d", arcGen)
      .attr("transform", "translate(250, 280)")
      .style('fill', (d) => d3.schemeCategory10[d.index % 10]);

    d3.select(node)
      .selectAll('#datum')
      .append('text')
      .attr("x", 550)
      .attr("y", (d, i) => labelScale(i))
      .text(d => d.data.name + ' (' + (d.data.value * 100).toFixed(2) + '%)');
    
    d3.select(node)
      .selectAll('#datum')
      .append('rect')
      .attr("x", 520)
      .attr("y", (d, i) => labelScale(i))
      .attr("width", 20)
      .attr("height", 20)
      .attr("transform", (d,i) => 'rotate(270,520,' + labelScale(i) + ')')
      .style('fill', (d) => d3.schemeCategory10[d.index % 10]);
  }

  render() {
    return(
      <Fragment>
          <div className="col-12">
            <h4>Majors as Percentage of Undergraduate Enrollment</h4>
          </div>
          <div className="col-12">
            <svg ref={node => this.node = node}
            width={this.state.width} height={this.state.height}>
            </svg>
          </div>
      </Fragment>
    );
  }
}

export default MajorPercentagesVisual;
