import React, {Fragment} from 'react';
import './Weather.css';
import {formatItem} from "../../utils";
import GridItem from "../item/GridItem";

class Weather extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			items: this.props.items
		}
	}

	componentDidMount() {
		console.log(this.state.items);
	}

	render() {
        return(
            <Fragment>
                <h4 style={{margin: "30px 0"}}>Weather Data</h4>
                <div className="row">
                    {this.state.items.map( (item, index) => {
                    	formatItem(item, "weather");
                        return <GridItem key={index} item={item} modelType={"weather"}/>
                    })}
                </div>
            </Fragment>
        );
    }
}

export default Weather;

