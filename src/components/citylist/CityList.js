import React from 'react';
import './CityList.css';
import ItemList from '../itemlist/ItemList';
import qs from "query-string";
import equal from "fast-deep-equal";
import {formatItem} from '../../utils';

const ENDPOINT = "https://api.scholarsearch.net/search/cities";

const CITY_FILTERS = [
  {
    key: "City Population",
    url_key: "population",
    options: ["< 250,000", "250,000 - 500,000", "500,000 - 750,000", "750,000 - 1,000,000", " > 1,000,000"],
    type: "numeric"
  },
  {
    key: "Monthly Cost of Living",
    url_key: "monthly_cost_of_living",
    options: ["< $10,000", "$10,000 - $20,000", "> $20,000"],
    type: "numeric"
  },
  {
    key: "Average Monthly Salary",
    url_key: "avg_monthly_salary",
    options: ["< $10,000", "$10,000 - $20,000", "> $20,000"],
    type: "numeric"
  },
  {
    key: "State",
    url_key: "state",
    options: ["AK", "AL", "AR", "AS", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MP", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UM", "UT", "VA", "VI", "VT", "WA", "WI", "WV", "WY"],
    type: "string"
  },
  {
    key: "Order By",
    url_key: "order_by",
    options: ["City Population", "Monthly Cost of Living", "Average Monthly Salary"],
    type: "order_by"
  },
  {
    key: "Order Direction",
    url_key: "order",
    options: ["Ascending", "Descending"],
    type: "order"
  }
];

class CityList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      pageNumber: qs.parse(this.props.location.search).page || 1,
      filters: qs.parse(this.props.location.search).filters,
      mins: qs.parse(this.props.location.search).mins,
      maxes: qs.parse(this.props.location.search).maxes,
      f2: qs.parse(this.props.location.search).f2,
      noresult: false,
      order_by: qs.parse(this.props.location.search).order_by,
      order: qs.parse(this.props.location.search).order
    };
  }

  makeApiCall(pageNumber, searchToken, filters, mins, maxes, f2, order_by, order) {
    console.log(`${ENDPOINT}?${pageNumber ? "&page=" + pageNumber : ""}${searchToken ? "&token=" + searchToken : ""}${filters ? "&filters=" + filters : ""}${mins ? "&mins=" + mins : ""}${maxes ? "&maxes=" + maxes : ""}${f2 ? "&f2=" + f2 : ""}${order_by ? "&order_by=" + order_by : ""}${order ? "&order=" + order : ""}`);
    fetch(`${ENDPOINT}?${pageNumber ? "&page=" + pageNumber : ""}${searchToken ? "&token=" + searchToken : ""}${filters ? "&filters=" + filters : ""}${mins ? "&mins=" + mins : ""}${maxes ? "&maxes=" + maxes : ""}${f2 ? "&f2=" + f2 : ""}${order_by ? "&order_by=" + order_by : ""}${order ? "&order=" + order : ""}`)
      .then((resp) => resp.json()) // Transform the data into json
      .then((data) => {
          // Process data
          let extras = data.extras[0];

          let ids = []
          Object.keys(data.extras[0]).forEach(key => {
            ids.push(key)
          });

          if(data.data.length === 0){
            //no results found
            console.log(data)
            this.setState({noresult: true});
          }

          data.data.forEach((city) => {

            let matches = data.extras[0][city.id] || ""
            formatItem(city, "cities", matches);

            city.additionalProperties = {
              "Age Restriction": city.event || "None"
            }

            city.matchText = extras && extras[city.id];
            city.matchQuery = searchToken;

            this.state.cities.push(city);
          });
      }).then(() => {
        this.setState({loaded: true});
      });
  }

  componentDidMount() {
    this.makeApiCall(this.state.pageNumber, this.state.searchToken, this.state.filters, this.state.mins, this.state.maxes, this.state.f2, this.state.order_by, this.state.order);
  }

  componentDidUpdate(prev) {
    if(!equal(prev.location.search, this.props.location.search)) {
      let temp = qs.parse(this.props.location.search).page || 1;
      let temp2 = qs.parse(this.props.location.search).token;
      let temp3 = qs.parse(this.props.location.search).filters;
      let temp4 = qs.parse(this.props.location.search).mins;
      let temp5 = qs.parse(this.props.location.search).maxes;
      let temp6 = qs.parse(this.props.location.search).f2;
      let temp7 = qs.parse(this.props.location.search).order_by;
      let temp8 = qs.parse(this.props.location.search).order;
      this.setState({
        pageNumber: temp,
        searchToken : temp2,
        filters: temp3,
        mins: temp4,
        maxes: temp5,
        f2: temp6,
        order_by: temp7,
        order: temp8,
        cities: []
      }, () => {
        this.makeApiCall(temp, temp2, temp3, temp4, temp5, temp6, temp7, temp8);
      });
    }
  }

  render() {
    if(this.state.noresult){
      return(
        <fragment>
          <h2>No results found</h2>
        </ fragment>
        );
    }
    return (
      <ItemList items={this.state.cities.length > 0 ? this.state.cities : null} title="Cities" type="grid" filters={CITY_FILTERS}
                modelType={this.props.location.pathname}  pageNumber={this.state.pageNumber} query={qs.parse(this.props.location.search).query}/>
    );
  }
}

export default CityList;
