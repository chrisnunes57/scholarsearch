import React from 'react';
import './CollegeList.css';
import ItemList from '../itemlist/ItemList';
import qs from "query-string";
import equal from 'fast-deep-equal';
import {formatItem} from "../../utils";

const ENDPOINT = "https://api.scholarsearch.net/search/colleges";
// const ITEMS_PER_PAGE = 15;

const COLLEGE_FILTERS = [
  {
    key: "In-State Tuition",
    url_key: "in_state_tuition",
    options: ["< $5,000", "$5,000 - $10,000", "> $10,000"],
    type: "dollar"
  },
  {
    key: "Out-Of-State Tuition",
    url_key: "out_of_state_tuition",
    options: ["< $10,000", "$10,000 - $20,000", "> $20,000"],
    type: "dollar"
  },
  {
    key: "Admission Rate",
    url_key: "admission_rate",
    options: ["< .1", ".1 - .25", ".25 - .5", "> .5"],
    type: "decimal"
  },
  {
    key: "Population",
    url_key: "undergrad_size",
    options: ["< 25,000", "25,000 - 50,000", "> 50,000"],
    type: "amount"
  },
  {
    key: "Financial Aid Given",
    url_key: "median_original_aid",
    options: ["< $10,000", "$10,000 - $20,000", "> $20,000"],
    type: "dollar"
  },
  {
    key: "Graduation Rate",
    url_key: "completion_rate",
    options: ["< .1", ".1 - .25", ".25 - .5", "> .5"],
    type: "decimal"
  },
  {
    key: "Order By",
    url_key: "order_by",
    options: ["In-State Tuition", "Out-Of-State Tuition", "Admission Rate", "Population", "Financial Aid Given", "Graduation Rate"],
    type: "order_by"
  },
  {
    key: "Order Direction",
    url_key: "order",
    options: ["Ascending", "Descending"],
    type: "order"
  }

];

class CollegeList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      colleges: [],
      pageNumber: qs.parse(this.props.location.search).page || 1,
      searchToken: qs.parse(this.props.location.search).token,
      filters: qs.parse(this.props.location.search).filters,
      mins: qs.parse(this.props.location.search).mins,
      maxes: qs.parse(this.props.location.search).maxes,
      f2: qs.parse(this.props.location.search).f2,
      noresult: false,
      order_by: qs.parse(this.props.location.search).order_by,
      order: qs.parse(this.props.location.search).order
    };
  }

  makeApiCall(pageNumber, searchToken, filters, mins, maxes, f2, order_by, order) {
    console.log(`${ENDPOINT}?${pageNumber ? "&page=" + pageNumber : ""}${searchToken ? "&token=" + searchToken : ""}${filters ? "&filters=" + filters : ""}${mins ? "&mins=" + mins : ""}${maxes ? "&maxes=" + maxes : ""}${f2 ? "&f2=" + f2 : ""}${order_by ? "&order_by=" + order_by : ""}${order ? "&order=" + order : ""}`);
    fetch(`${ENDPOINT}?${pageNumber ? "&page=" + pageNumber : ""}${searchToken ? "&token=" + searchToken : ""}${filters ? "&filters=" + filters : ""}${mins ? "&mins=" + mins : ""}${maxes ? "&maxes=" + maxes : ""}${f2 ? "&f2=" + f2 : ""}${order_by ? "&order_by=" + order_by : ""}${order ? "&order=" + order : ""}`)
     .then((resp) => resp.json()) // Transform the data into json
      .then((data) => {
          let extras = data.extras[0];
          // Process data
          let ids = []
          Object.keys(data.extras[0]).forEach(key => {
            ids.push(key)
          });

          //console.log("IDs: " + ids)

          if(data.data.length === 0){
            //no results found
            console.log(data)
            this.setState({noresult: true});
          }

          console.log("skip")

          data.data.forEach((college) => {
            let matches = data.extras[0][college.id] || ""
            formatItem(college, "colleges", matches);

            college.matchText = extras && extras[college.id];
            college.matchQuery = searchToken;

            this.state.colleges.push(college);
          }
          );

          
      }).then(() => {
        this.setState({loaded: true});
      });
  }

  componentDidMount() {
    this.makeApiCall(this.state.pageNumber, this.state.searchToken, this.state.filters, this.state.mins, this.state.maxes, this.state.f2, this.state.order_by, this.state.order);
  }

  componentDidUpdate(prev) {
    if(!equal(prev.location.search, this.props.location.search)) {
      let temp = qs.parse(this.props.location.search).page || 1;
      let temp2 = qs.parse(this.props.location.search).token;
      let temp3 = qs.parse(this.props.location.search).filters;
      let temp4 = qs.parse(this.props.location.search).mins;
      let temp5 = qs.parse(this.props.location.search).maxes;
      let temp6 = qs.parse(this.props.location.search).f2;
      let temp7 = qs.parse(this.props.location.search).order_by;
      let temp8 = qs.parse(this.props.location.search).order;
      this.setState({
        pageNumber: temp,
        searchToken : temp2,
        filters: temp3,
        mins: temp4,
        maxes: temp5,
        f2: temp6,
        order_by: temp7,
        order: temp8,
        colleges: []
      }, () => {
        this.makeApiCall(temp, temp2, temp3, temp4, temp5, temp6, temp7, temp8);
      });
    }
  }

  render() {
    if(this.state.noresult){
      return(
        <fragment>
          <h2>No results found</h2>
        </ fragment>  
        );
    }
    return (
      <ItemList items={this.state.colleges.length > 0 ? this.state.colleges : null} title="Colleges" type="grid" filters={COLLEGE_FILTERS}
                modelType={this.props.location.pathname} pageNumber={this.state.pageNumber} query={qs.parse(this.props.location.search).query}/>
    );
  }
}

export default CollegeList;
