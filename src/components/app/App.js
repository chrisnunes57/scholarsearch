import React from 'react';
import './App.css';
import Navigation from '../navigation/Navigation';
import HomePage from '../homepage/HomePage';
import Colleges from '../collegelist/CollegeList';
import Events from '../eventlist/EventList';
import Cities from '../citylist/CityList';
import NotFound from '../notfound/NotFound';
import About from '../about/About';
import ItemView from '../itemview/ItemView';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Search from "../search/Search";
import Visuals from "../visuals/Visuals";

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Navigation />
          {/* This "switch" looks at the requested URL and loads the corresponding react component */}
          <Switch>
            <Route path="/" exact component={HomePage} />
            <Route path="/colleges" exact component={Colleges} />
            <Route path="/colleges/:slug" component={ItemView} />
            <Route path="/events" exact component={Events} />
            <Route path="/events/:slug" component={ItemView} />
            <Route path="/cities" exact component={Cities} />
            <Route path="/cities/:slug" component={ItemView} />
            <Route path="/about" component={About} />
            <Route path="/search" component={Search} />
            <Route path="/visuals" component={Visuals}/>
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
