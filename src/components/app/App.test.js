import renderer from 'react-test-renderer';
import About from "../about/About";
import CityList from "../citylist/CityList";
import CollegeList from "../collegelist/CollegeList";
import EventList from "../eventlist/EventList";
import HomePage from "../homepage/HomePage";
import Navigation from "../navigation/Navigation";
import NotFound from "../notfound/NotFound";
import GridItem from "../item/GridItem";
import TeamCard from "../item/TeamCard";
import ItemView from "../itemview/ItemView";

// Selenium Imports
const { Builder, By, Key, until } = require('selenium-webdriver');
require('selenium-webdriver/firefox');
require('geckodriver');

const devURL = 'http://localhost:3000';
const prodURL = 'https://scholarsearch.net';

// We can change the root url based on what we want to test for
const rootURL = prodURL;

const d = new Builder().forBrowser('firefox').build();
const waitUntilTime = 20000;
let driver, el, actual, expected;
jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 60 * 5;

// Utility function to get elements by CSS constructor
// Based on other similar functions here: https://medium.com/@mathieux51/jest-selenium-webdriver-e25604969c6
async function getElementByCSS(selector) {
    const el = await driver.wait(until.elementLocated(By.css(selector)), waitUntilTime);
    return await driver.wait(until.elementIsVisible(el), waitUntilTime);
}

describe("initialize Selenium webdriver", () => {
  it('waits for the driver to start', () => {
    return d.then(_d => {
          driver = _d;
    });
  });

  it('initialises the context', async () => {
      await driver.manage().window().setPosition(0, 0);
      await driver.manage().window().setSize(1280, 1024);
      await driver.get(rootURL);
    });
});


describe("Component Snapshot Tests", () => {

  it("should match About snapshot", () => {
    const component = renderer.create(About);
    let about = component.toJSON();
    expect(about).toMatchSnapshot();
  });

  it("should match CityList snapshot", () => {
    const component = renderer.create(CityList);
    let citylist = component.toJSON();
    expect(citylist).toMatchSnapshot();
  });

  it("should match CollegeList snapshot", () => {
    const component = renderer.create(CollegeList);
    let collegelist = component.toJSON();
    expect(collegelist).toMatchSnapshot();
  });

  it("should match EventList snapshot", () => {
    const component = renderer.create(EventList);
    let eventlist = component.toJSON();
    expect(eventlist).toMatchSnapshot();
  });

  it("should match HomePage snapshot", () => {
    const component = renderer.create(HomePage);
    let homePage = component.toJSON();
    expect(homePage).toMatchSnapshot();
  });

  it("should match Navigation snapshot", () => {
    const component = renderer.create(Navigation);
    let nav = component.toJSON();
    expect(nav).toMatchSnapshot();
  });

  it("should match FilterList snapshot", () => {
    const component = renderer.create(NotFound);
    let notfound = component.toJSON();
    expect(notfound).toMatchSnapshot();
  });

  it("should match GridItem snapshot", () => {
    const component = renderer.create(GridItem);
    let grid = component.toJSON();
    expect(grid).toMatchSnapshot();
  });

  it("should match TeamCard snapshot", () => {
    const component = renderer.create(TeamCard);
    let card = component.toJSON();
    expect(card).toMatchSnapshot();
  });

  it("should match NearbyCities snapshot", () => {
    const component = renderer.create(ItemView);
    let item = component.toJSON();
    expect(item).toMatchSnapshot();
  });

});

describe("Selenium testing GUI", () => {

    // reusable element
    let el = null;
    it('should have a title', async () => {
        el = await getElementByCSS(".parallax-title.display-4");

        let actual = await el.getText();
        let expected = 'CHOOSE YOUR BEST FUTURE';
        expect(actual).toEqual(expected);
    });

    it('should have navigation', async () => {
        el = await getElementByCSS(".nav-link");

        let actual = await el.getText();
        let expected = 'Colleges';
        expect(actual).toEqual(expected);
    });

    it('should be able to get to colleges tab from navbar click', async () => {
        el.click();
        // Test to see if team cards are populated
        let temp = await getElementByCSS(".team-card:first-child");
        expect(temp).toBeTruthy();
    });

    it('navigates to about page', async () => {
        await driver.get(rootURL + "/about");

        expect(true).toBeTruthy();
    });

    it('loads team cards', async () => {
        // Test to see if team cards are populated
        let temp = await getElementByCSS(".team-card");
        expect(temp).toBeTruthy();
    });

    it('events tab loads', async () => {
        await driver.get(rootURL + "/events");
        // Test to see if title exists
        let el = await getElementByCSS("h1");
        let actual = await el.getText();
        let expected = 'Events';
        expect(actual).toEqual(expected);
    });

    it('cities tab loads', async () => {
        await driver.get(rootURL + "/cities");
        // Test to see if title exists
        let el = await getElementByCSS("h1");
        let actual = await el.getText();
        let expected = 'Cities';
        expect(actual).toEqual(expected);
    });

});

describe("Selenium webdriver should shut down at the end" , () => {
    it('should shut down at the end', async () => {
        driver.quit();
    });
})
