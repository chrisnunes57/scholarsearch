import React from 'react';
import SplashImage from '../../assets/homepage/splash_image.jpg';
import MoneyImage from '../../assets/homepage/money_image.jpg';
//import ConcertImage from '../../assets/homepage/concert_image.jpg';
import './HomePage.css';
import ParallaxBlock from './ParallaxBlock';
import FancySpacer from './FancySpacer';
import Debt from '../../assets/icons/debt.svg';
import Bank from '../../assets/icons/bank.svg';
import Handshake from '../../assets/icons/handshake.svg';
import City from '../../assets/icons/City.png';
import College from '../../assets/icons/college.png';
import Event from '../../assets/icons/event.png';

function HomePage() {
  return (
    <section className="homepage-wrapper">
      <ParallaxBlock backgroundImage={SplashImage}  heroTitle="Choose your best future"/>
              <div className="imagelink">
          <a href= "https://scholarsearch.net/cities">
            <img src={City} alt="list of city model pages"/>
          </a>
          <a href= "https://scholarsearch.net/colleges">
            <img src={College} alt="list of college model pages"/>
          </a>
          <a href= "https://scholarsearch.net/events">
            <img src={Event} alt="list of Event model pages"/>

          </a>
        </div>
      <div className="container homepage-content">
        <p className="lead">Choosing a college shouldn't be a hassle. We save you the work of researching and comparing different universities across country. Instead, we look at what you value in an institution and match you with a college that will work for you. In addition to looking at the price and value of each degree, we factor in information about the city around the college to ensure that you will find a good fit.</p>
        <FancySpacer />
      </div>
      <ParallaxBlock backgroundImage={MoneyImage} heroTitle="Find affordable college programs"/>
      <div className="container homepage-content">
        <div className="row">
          <div className="col-md-4 icon-wrapper">
            <img src={Bank} alt=""/>
            <h4 className="icon-title">Compare Costs</h4>
            <p className="icon-description">Compare and contrast the total value of universities, including financial aid and scholarships.</p>
          </div>
          <div className="col-md-4 icon-wrapper">
            <img src={Debt} alt=""/>
            <h4 className="icon-title">Avoid Debt</h4>
            <p className="icon-description">The average student debt is $38,390 and rising. Learn which colleges will be most affordable for your budget.</p>
          </div>
          <div className="col-md-4 icon-wrapper">
            <img src={Handshake} alt=""/>
            <h4 className="icon-title">Navigate Finances</h4>
            <p className="icon-description">Work together with us to discover which college education will offer the best return on your investment.</p>
          </div>
        </div>
      </div>
      {/* <ParallaxBlock backgroundImage={ConcertImage} heroTitle="Locations that suit your lifestyle"/>
      <h1>Content</h1>
      <h1>Content</h1>
      <h1>Content</h1>
      <h1>Content</h1>
      <h1>Content</h1>
      <h1>Content</h1>
      <h1>Content</h1>
      <h1>Content</h1>
      <h1>Content</h1> */}
    </section>
  );
}

export default HomePage;
