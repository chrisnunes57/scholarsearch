import React from 'react';
import './HomePage.css';

function FancySpacer() {
  return (
    <span>
      <hr width="50%" className="fancy" />
    </span>
  );
}

export default FancySpacer;
