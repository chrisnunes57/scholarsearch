import React from 'react';
import './HomePage.css';

class ParallaxBlock extends React.Component {
  render() {
    return (
      <div className="parallax-wrapper" style={{backgroundImage: `url(${this.props.backgroundImage})`}}>
        <h1 className="parallax-title display-4">{this.props.heroTitle}</ h1>
      </div>
    );
  }
}

export default ParallaxBlock;
