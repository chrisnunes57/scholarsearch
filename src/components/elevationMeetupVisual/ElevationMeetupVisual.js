import React from 'react';
import './ElevationMeetupVisual.css';
import * as d3 from 'd3';

const QUERY = "https://api.after-work.site/meetups"
const QUERY_TWO = "https://api.after-work.site/cities"

class ElevationMeetupVisual extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      width: props.width || null,
      height: props.height || null
    };
    this.updateVisual = this.updateVisual.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.updateVisual();
  }

  componentDidUpdate(){
    this.updateVisual();
  }

  getData(){
  
  }

  getEventData(){
    return fetch(QUERY)
      .then((resp) => {
        return resp.json();
      });
  }

  getCityData(){
    return fetch(QUERY_TWO)
      .then((resp) => {
        return resp.json();
      });
  }

  getEventAndCityData(){
    return Promise.all([this.getEventData(), this.getCityData()]);
  }

  updateVisual(){
    const node = this.node;

    this.getEventAndCityData()
      .then((data) => {
        var populationMeetupCount = {};
        data[0].forEach((item) => {
          var key = "" + item.city_name; 
          if(! (key in populationMeetupCount)){            
            populationMeetupCount[key] = {
              name: key,
              count: 1,
            };
          }
          else{
            populationMeetupCount[key].count += 1; 
          }
        })
        data[1].forEach((item) => {
          var key = "" + item.name; 
          if(key in populationMeetupCount){            
            populationMeetupCount[key].population = item.elevationMeters;
          }
        })
    var count = [];
    var keys = [];
    var population = [];
    for(var key in populationMeetupCount){
      if(populationMeetupCount[key].population != null){
        keys.push(populationMeetupCount[key].name);
        count.push(populationMeetupCount[key].count);
        population.push(populationMeetupCount[key].population);
      }
    }

    //Spacing variables
    var paddingLeft = 50;
    var paddingBottom = 100;
    var paddingTop = 100;
    var paddingRight = 30;

    const countMax = d3.max(count);
    const populationMax = d3.max(population);

    const xScale = d3.scaleLinear()
      .domain([0, populationMax])
      .range([paddingLeft, this.state.width - paddingRight]);

    const yScale = d3.scaleLinear()
      .domain([0, countMax])
      .range([this.state.height - paddingBottom, paddingTop]);

    const xAxis = d3.axisBottom(xScale);

    const yAxis = d3.axisLeft(yScale);

    //Add y axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'yAxis')
      .attr('transform', 'translate(' + paddingLeft + ',0)')
      .call(yAxis);

    //Add x axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'xAxis')
      .attr('transform', 'translate(0, ' + (this.state.height - paddingBottom) + ')')
      .call(xAxis);

    d3.select(node)
      .selectAll('#xAxis')
      .selectAll('.tick')
      .attr('opacity', '0.99')
    
    d3.select(node)
      .append('text')
      .attr('x', xScale(populationMax / 2))
      .attr('y', this.state.height - 60)
      .attr('text-anchor', 'middle')
      .text('Elevation (meters)');

    d3.select(node)
      .append('text')
      .attr('x', 30)
      .attr('y', yScale(countMax / 2))
      .attr('text-anchor', 'middle')
      .attr('transform', 'rotate(270, 30,' + yScale(countMax/2) + ')')
      .text('Number of Meetups');
    
    //Link the data to g tags and add a child rect
    d3.select(node)
      .selectAll('#point')
      .data(keys)
      .enter()
      .append('g')
      .attr('id', 'point')
      .append('circle')
      .style('fill', '#007bff')
      .attr('cx', (d) => xScale(populationMeetupCount[d].population))
      .attr('cy', (d) => yScale(populationMeetupCount[d].count))
      .attr('r', 5);

    //Add a child text tag with the number of colleges
    {/*d3.select(node)
      .selectAll('#point')
      .append('text')
      .attr('x', d => xScale(populationMeetupCount[d].population))
      .attr('y', d => yScale(populationMeetupCount[d].count) - 30)
      .attr('text-anchor', 'middle')
      .attr('transform', d => 'rotate(270,' + xScale(populationMeetupCount[d].population) + ', ' + (yScale(populationMeetupCount[d].count) - 30) + ')')
      .text(d => "" + d);*/}
    });
  }

  render() {
    return(
      <svg ref={node => this.node = node}
      width={this.state.width} height={this.state.height}>
      </svg>
    );
  }

}

export default ElevationMeetupVisual;
