import React, {Fragment} from 'react';
import './NearbyCities.css';
import {formatItem} from "../../utils";
import GridItem from "../item/GridItem";

const API_ENDPOINT = "https://api.scholarsearch.net/cities/query?id=";

class NearbyCities extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            itemIds: this.props.items,
            items: [],
            numLoaded: 0
        }
    }

    componentDidMount() {
        // Here we need to go through all of our item ids and update them with items
        this.state.itemIds.forEach(id => {
            this.fetchData(id);
        })
    }

    fetchData(id) {
        fetch(`${API_ENDPOINT}${id}`)
            .then((resp) => {
                return resp.json();
            }) // Transform the data into json
            .then((data) => {
                formatItem(data.data[0], "cities");
                this.state.items.push(data.data[0]);
                this.setState({numLoaded: this.state.numLoaded + 1})
            });
    }

    render() {
        return(
            <Fragment>
                <h4 style={{margin: "30px 0"}}>Nearby Cities</h4>
                <div className="row">
                    {this.state.items.map( (item, index) => {
                        return <GridItem key={index} item={item} modelType={"cities"}/>
                    })}
                </div>
            </Fragment>
        );
    }
}

export default NearbyCities;
