import React from 'react';
import logo from '../../assets/logo.svg';
import './Navigation.css';
import { Link } from "react-router-dom";

function Navigation() {
  return (
    <nav className="navbar navbar-expand-lg">
        <Link className="navbar-brand" to="/">
            <img src={logo} alt="ScholarSearch Logo" />
        </Link>
        <div className="navbar-nav-scroll">
            <Link className="navbar-brand" to="/">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        ScholarSearch
                    </li>
                </ul>
            </Link>
        </div>
        <ul className="navbar-nav ml-auto">
            <li className="nav-item">
                <Link className="nav-link" to="/colleges">Colleges</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/events">Events</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/cities">Cities</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/about">About</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/search">Search</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/visuals">Visuals</Link>
            </li>
        </ul>
    </nav>
  );
}

export default Navigation;
