import React from 'react';
import './MusicHeatMap.css';
import * as d3 from 'd3';
import stateData from './states';
import eventCount from './eventCount';
import locations from './locations';

const QUERY = ""

class MusicHeatMap extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      width: props.width || null,
      height: props.height || null
    };
    this.updateVisual = this.updateVisual.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.updateVisual();
  }

  componentDidUpdate(){
    this.updateVisual();
  }

  getData(){
    
  }

  updateVisual(){
    const node = this.node;

    var names = [];
    var rapData = [];
    var rockData = [];
    var latinData = [];
    var racingData = [];
    var popData = [];
    var musicData = [];
    var rbData = [];
    var countryData = [];
    var religiousData = [];
    var childData = [];
    var metalData = [];
    var otherData = [];
    var altData = [];
    var danceData = [];
    var latitudes = [];
    var longitudes = [];
    
    console.log(eventCount);
    for(var key in eventCount){
      names.push(key);
      rapData.push(eventCount[key]["Hip-Hop/Rap"]);
      rockData.push(eventCount[key]["Rock"]);
      latinData.push(eventCount[key]["Latin"]);
      racingData.push(eventCount[key]["Motorsports/Racing"]);
      popData.push(eventCount[key]["Pop"]);
      musicData.push(eventCount[key]["Music"]);
      rbData.push(eventCount[key]["R&B"]);
      countryData.push(eventCount[key]["Country"]);
      religiousData.push(eventCount[key]["Religious"]);
      childData.push(eventCount[key]["Children's Music"]);
      metalData.push(eventCount[key]["Metal"]);
      otherData.push(eventCount[key]["Other"]);
      altData.push(eventCount[key]["Alternative"]);
      danceData.push(eventCount[key]["Dance/Electronic"]);
    }

    var genres = [
      "Hip-Hop/Rap",
      "Rock",
      "Latin",
      "Motorsports/Racing",
      "Pop",
      "Music",
      "R&B",
      "Country",
      "Religious",
      "Children's Music",
      "Metal",
      "Other",
      "Alternative",
      "Dance/Electronic"]

    var allGenreData = [rapData, rockData, latinData, racingData, popData, musicData, rbData, countryData, religiousData, childData, metalData, otherData, altData, danceData];

    for(var i = 0; i < locations.data.length; i++){
      latitudes.push(locations.data[i].latitude);
      longitudes.push(locations.data[i].longitude * -1);
    }
    
    //Spacing variables
    var paddingLeft = 60;
    var paddingBottom = 150;
    var paddingTop = 100;

    const projection = d3.geoAlbersUsa()
      .scale(this.state.width)
      .translate([this.state.width/2,this.state.height/2]);

    const path = d3.geoPath()
      .projection(projection);

    var dataMax = 20;

    const cScale = d3.scalePow()
      .exponent(0.5)
      .domain([0,dataMax])
      .range([0, 50]);

    const labelScale = d3.scaleLinear()
      .domain([0, genres.length])
      .range([0, 300]);

    d3.select(node)
      .selectAll("path")
      .data(stateData.features)
      .enter()
      .append("path")
      .attr("id", "state")
      .attr("d", path)
      .attr("stroke", 'black')
      .attr("fill", "#e3e3e3");

    d3.select(node)
      .selectAll("g")
      .data(names)
      .enter()
      .append("g")
      .attr("id", "datum");
    
    for(var j = 0; j < allGenreData.length; j++){
      d3.select(node)
        .selectAll('#datum')
        .append('circle')
        .attr("cx", (d, i) => {return projection([longitudes[i], latitudes[i]])[0]})
        .attr("cy", (d, i) => {return projection([longitudes[i], latitudes[i]])[1]})
        .attr("r", (d, i) => cScale(allGenreData[j][i]))
        .attr("fill", d3.schemeCategory10[j%10]);
    }
    
    d3.select(node)
      .selectAll('#legend')
      .data(genres)
      .enter()
      .append('g')
      .attr('id', 'legend')

    d3.select(node)
      .selectAll('#legend')
      .append('text')
      .attr("x", 30)
      .attr("y", (d, i) => labelScale(i))
      .attr("font-size", "14")
      .attr("transform", "translate(950, 400)")
      .text(d =>  d);
    
    d3.select(node)
      .selectAll('#legend')
      .append('rect')
      .attr("x", 0)
      .attr("y", (d, i) => labelScale(i))
      .attr("width", 20)
      .attr("height", 20)
      .attr("transform", "")
      .attr("transform", (d,i) => 'translate(950, 400), rotate(270,0,' + labelScale(i) + ')')
      .style('fill', (d, i) => d3.schemeCategory10[i % 10]);
  }

  render() {
    return(
      <svg ref={node => this.node = node}
      width={this.state.width} height={this.state.height}>
      </svg>
    );
  }

}

export default MusicHeatMap;
