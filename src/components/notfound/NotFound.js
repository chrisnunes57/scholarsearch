import React from 'react';
import './NotFound.css';

function NotFound() {
  return (
    <h1>Error 404: No page exists for {window.location.pathname}</h1>
  );
}

export default NotFound;
