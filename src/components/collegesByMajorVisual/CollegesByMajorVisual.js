import React from 'react';
import './CollegesByMajorVisual.css';
import {formatItem} from '../../utils';
import * as d3 from 'd3';

const QUERY = "https://api.scholarsearch.net/search/colleges"

class CollegesByMajorVisual extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      width: props.width || null,
      height: props.height || null
    };
    this.updateVisual = this.updateVisual.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.updateVisual();
  }

  componentDidUpdate(){
    this.updateVisual();
  }

  getData(){
    
  }

  getCollegeData(num){
    return fetch(QUERY + num)
      .then((resp) => {
        return resp.json();
      });
  }

  getAllCollegeData(num){
    var promiseArray = []
    for(var i = 1; i <= num; i++){
      promiseArray.push(this.getCollegeData(i));
    }
    return Promise.all(promiseArray);
  }

  updateVisual(){
    const node = this.node

    var popularMajorCount = {};

    //Get college data
    return fetch(QUERY)
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        data.data.forEach((item) => {
          formatItem(item, "colleges");
          var key = "" + item.popular_major; 
          if(! (key in popularMajorCount)){            
            popularMajorCount[key] = 1;
          }
          else{
            popularMajorCount[key] += 1; 
          }
        });
      })
      .then(() => {
    var data = [];
    var keys = [];
    for(var key in popularMajorCount){
      keys.push(key);
      data.push(popularMajorCount[key]);
    }

    //Spacing variables
    var paddingLeft = 60;
    var paddingBottom = 150;
    var paddingTop = 100;

    const dataMax = d3.max(data);
    
    const xScale = d3.scaleBand()
      .domain(keys)
      .range([paddingLeft, this.state.width])
      .padding(0.1);
    
    const yScale = d3.scaleLinear()
      .domain([0, dataMax])
      .range([this.state.height - paddingBottom, paddingTop]);
    
    const yAxis = d3.axisLeft(yScale)

    const xAxis = d3.axisBottom(xScale);

    //Add y axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'yAxis')
      .attr('transform', 'translate(' + paddingLeft + ',0)')
      .call(yAxis);

    //Add x axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'xAxis')
      .attr('transform', 'translate(0, ' + (this.state.height - paddingBottom) + ')')
      .call(xAxis);
    
    d3.select(node)
      .selectAll('#xAxis')
      .selectAll('.tick')
      .selectAll('text')
      .attr('dx', -10)
      .attr('dy', 0)
      .attr('text-anchor', 'end')
      .attr('transform', 'rotate(315)')
    
    d3.select(node)
      .append('text')
      .attr('x', 30)
      .attr('y', yScale(dataMax / 2))
      .attr('text-anchor', 'middle')
      .attr('transform', 'rotate(270, 30,' + yScale(dataMax/2) + ')')
      .text('Number of Colleges');
    
    d3.select(node)
      .append('text')
      .attr('x', (this.state.width - paddingLeft) / 2)
      .attr('y', (this.state.height - 30))
      .attr('text-anchor', 'middle')
      .text('Major');

    //Link the data to g tags and add a child rect
    d3.select(node)
      .selectAll('#bar')
      .data(data)
      .enter()
      .append('g')
      .attr('id', 'bar')
      .append('rect')
      .style('fill', '#007bff')
      .attr('x', (d,i) => xScale(keys[i]))
      .attr('y', d => yScale(d))
      .attr('height', d => this.state.height - yScale(d) - paddingBottom)
      .attr('width', xScale.bandwidth())
      .attr('stroke-width', 3)
      .attr('stroke', '#000000');
   
    //Add a child text tag with the number of colleges
    d3.select(node)
      .selectAll('#bar')
      .append('text')
      .attr('x', (d,i) => xScale(keys[i]) + xScale.bandwidth() * 0.5)
      .attr('y', d => yScale(d) - 30)
      .attr('text-anchor', 'middle')
      .text(d => "" + d);

    });

  }

  render() {
    return(
      <svg ref={node => this.node = node}
      width={this.state.width} height={this.state.height}>
      </svg>
    );
  }

}

export default CollegesByMajorVisual;
