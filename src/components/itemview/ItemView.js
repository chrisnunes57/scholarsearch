import React from 'react';
import './ItemView.css';
import {formatItem, getLocation} from '../../utils';
// import { Link } from "react-router-dom";
import ImportantProperties from '../item/ImportantProperties';
import AdditionalProperties from '../item/AdditionalProperties';
// import EventCostByStateVisual from "../eventCostByStateVisual/EventCostByStateVisual";
// import CollegesByMajorVisual from "../collegesByMajorVisual/CollegesByMajorVisual";
import CostOfLivingVisual from "../costOfLivingVisual/CostOfLivingVisual";
import MajorPercentagesVisual from "../majorPercentagesVisual/MajorPercentagesVisual";
import NearbyColleges from "../nearbyColleges/NearbyColleges";
import NearbyCities from "../nearbyCities/NearbyCities";
import NearbyEvents from "../nearbyEvents/NearbyEvents";
import Weather from "../weather/Weather"

const PLACEHOLDER = "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22288%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20288%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16d8e67221e%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16d8e67221e%22%3E%3Crect%20width%3D%22288%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2296.32500076293945%22%20y%3D%22118.8%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";

class ItemView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        item: props.location.item || null,
        apiEndpoint: `https://api.scholarsearch.net`,
        apiQuery: window.location.pathname,
        modelType: props.modelType || window.location.pathname.split('/')[1]
    }
  }

  componentDidMount() {
    if(!this.state.item) {
      this.fetchItemInformation(); // Who's a good boy! Fetch!
    }
  }

  componentDidUpdate() {
    if (this.state.apiQuery !== window.location.pathname) {
      window.location.reload(false);
    }
  }

  fetchItemInformation() {
      fetch(`${this.state.apiEndpoint}${this.state.apiQuery}`)
          .then((resp) => {
              return resp.json();
          }) // Transform the data into json
          .then((data) => {
              formatItem(data.data, this.state.modelType);
              this.setState({item: data});
          });
  }

  formatWeather(weather) {
    var items = [];
    console.log(weather[0].weather_entries);
    weather[0].weather_entries.forEach(item =>{
      items.push(item);
    });
    return items;
  }

  render() {
    let item = this.state.item;
    console.log(item)

    return(
        item ? <section className="container">
            <div className="row">
                <div className="col-md">
                    <img src={item.data.img_src || item.data.image_url || PLACEHOLDER} alt={item.data.imgAlt}/>
                    <p className="text-left subtitle" style={{padding: "4px 0 0 4px"}}>{getLocation(item.data)}</ p>
                </div>
                <div className="col-md text-left" style={{paddingLeft: "40px" }}>
                    <h3 style={{marginBottom: "30px"}}>{item.data.name}</h3>
                    <ImportantProperties item={item.data} />
                </div>
            </div>
            {item.data.additionalProperties && <div className="row">
                <div className="col-md">
                    <h4 style={{margin: "30px 0"}}>Additional Properties</h4>
                    <AdditionalProperties item={item.data} />
                </div>
            </div>}
            {this.state.modelType === "cities" && item.data.costs && <div className="row"><CostOfLivingVisual width="1000" height="600" costData={item.data.costs}/></div>}
            {this.state.modelType === "colleges" && item.data.majors && <div className="row"><MajorPercentagesVisual width="1000" height="600" majorData={item.data.majors}/></div>}
            {item.data.cities && <NearbyCities items={item.data.cities}/>}
            {item.data.city && <NearbyCities items={[item.data.city]}/>}
            {item.data.colleges && <NearbyColleges items={item.data.colleges}/>}
            {item.data.events && <NearbyEvents items={item.data.events}/>}
            {item.data.weather && <Weather items={this.formatWeather(item.data.weather)}/>}
        </section>

        : <h1>Loading</h1>
    );
  }
}

export default ItemView;
