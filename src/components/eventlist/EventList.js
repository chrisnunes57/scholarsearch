import React from 'react';
import './EventList.css';
import ItemList from '../itemlist/ItemList';
import qs from "query-string";
import equal from "fast-deep-equal";
import {formatItem} from "../../utils";

const ENDPOINT = "https://api.scholarsearch.net/search/events";

const EVENT_FILTERS = [
  {
    key: "Venue",
    url_key: "venue",
    options: [],
    type: "string"
  },
  {
    key: "Event Type",
    url_key: "event_type",
    options: [],
    type: "string"
  },
  {
    key: "Admission Cost",
    url_key: "cost",
    options: ["< $50, $50 - $100", "$100 - $150", "> $150"],
    type: "numeric"
  },
  {
    key: "Order By",
    url_key: "order_by",
    options: ["Time", "Admission Cost"],
    type: "order_by"
  },
  {
    key: "Order Direction",
    url_key: "order",
    options: ["Ascending", "Descending"],
    type: "order"
  }
];

class EventList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      events: [],
      pageNumber: qs.parse(this.props.location.search).page || 1,
      filters: qs.parse(this.props.location.search).filters,
      mins: qs.parse(this.props.location.search).mins,
      maxes: qs.parse(this.props.location.search).maxes,
      f2: qs.parse(this.props.location.search).f2,
      noresult: false,
      order_by: qs.parse(this.props.location.search).order_by,
      order: qs.parse(this.props.location.search).order
    };
  }

  makeApiCall(pageNumber, searchToken, filters, mins, maxes, f2, order_by, order) {
    console.log(`${ENDPOINT}?${pageNumber ? "&page=" + pageNumber : ""}${searchToken ? "&token=" + searchToken : ""}${filters ? "&filters=" + filters : ""}${mins ? "&mins=" + mins : ""}${maxes ? "&maxes=" + maxes : ""}${f2 ? "&f2=" + f2 : ""}${order_by ? "&order_by=" + order_by : ""}${order ? "&order=" + order : ""}`);
    fetch(`${ENDPOINT}?${pageNumber ? "&page=" + pageNumber : ""}${searchToken ? "&token=" + searchToken : ""}${filters ? "&filters=" + filters : ""}${mins ? "&mins=" + mins : ""}${maxes ? "&maxes=" + maxes : ""}${f2 ? "&f2=" + f2 : ""}${order_by ? "&order_by=" + order_by : ""}${order ? "&order=" + order : ""}`)

      .then((resp) => resp.json()) // Transform the data into json
      .then((data) => {
          let extras = data.extras[0];
          // Process data
          let ids = []
          Object.keys(data.extras[0]).forEach(key => {
            ids.push(key)
          });

          if(data.data.length === 0){
            //no results found
            console.log(data)
            this.setState({noresult: true});
          }

          data.data.forEach((event) => {

            let matches = data.extras[0][event.id] || ""
            formatItem(event, "events", matches);

            event.matchText = extras && extras[event.id];
            event.matchQuery = searchToken;

            this.state.events.push(event);
          });
      }).then(() => {
        this.setState({loaded: true});
      });
  }

  componentDidMount() {
    this.makeApiCall(this.state.pageNumber, this.state.searchToken, this.state.filters, this.state.mins, this.state.maxes, this.state.f2, this.state.order_by, this.state.order);
  }

  componentDidUpdate(prev) {
    if(!equal(prev.location.search, this.props.location.search)) {
      let temp = qs.parse(this.props.location.search).page || 1;
      let temp2 = qs.parse(this.props.location.search).token;
      let temp3 = qs.parse(this.props.location.search).filters;
      let temp4 = qs.parse(this.props.location.search).mins;
      let temp5 = qs.parse(this.props.location.search).maxes;
      let temp6 = qs.parse(this.props.location.search).f2;
      let temp7 = qs.parse(this.props.location.search).order_by;
      let temp8 = qs.parse(this.props.location.search).order;
      this.setState({
        pageNumber: temp,
        searchToken : temp2,
        filters: temp3,
        mins: temp4,
        maxes: temp5,
        f2: temp6,
        order_by: temp7,
        order: temp8,
        events: []
      }, () => {
        this.makeApiCall(temp, temp2, temp3, temp4, temp5, temp6, temp7, temp8);
      });
    }
  }

  render() {
    if(this.state.noresult){
      return(
        <fragment>
          <h2>No results found</h2>
        </ fragment>  
        );
    }
    return (
      <ItemList items={this.state.events.length > 0 ? this.state.events : null} title="Events" type="grid" filters={EVENT_FILTERS}
                modelType={this.props.location.pathname}  pageNumber={qs.parse(this.props.location.search).page} query={qs.parse(this.props.location.search).query}/>
    );
  }
}

export default EventList;
