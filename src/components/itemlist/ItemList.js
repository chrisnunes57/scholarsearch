import React, {Fragment} from 'react';
import './ItemList.css';
import GridItem from '../item/GridItem';
import TeamCard from '../item/TeamCard';
import Pagination from '../pagination/Pagination';
import equal from 'fast-deep-equal';
import Loading from "../loading/Loading";
import { Redirect } from "react-router-dom";
import FilterList from "../filters/FilterList";

class ItemList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      items: props.items || null,
      title: props.title || "",
      type: props.type ? props.type.toLowerCase() : "",
      pageNumber: this.props.pageNumber || 1,
      searchValue: "",
      newQuery: false,
      lastQuery: "",
      modelType: this.props.modelType,
      currentFilters: {}
    };

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateFilters = this.updateFilters.bind(this);
  }

  updateFilters(key, realKey, value, type="") {
    let temp = this.state.currentFilters;
    temp[key] = {
      value: value,
      realKey: realKey,
      type: type
    };
    this.setState({currentFilters: temp}, () => {
    });
  }

  componentDidUpdate(prev) {
    if(!equal(prev.items, this.props.items)) {
      this.setState({
        items: this.props.items,
        newQuery: false,
        currentFilters: {},
        lastQuery: window.location.search
      })
    }

    if(prev.pageNumber !== this.props.pageNumber) {
      this.setState({
        pageNumber: this.props.pageNumber,
        lastQuery: window.location.search
      });
    }

  }

  handleInput(e) {
    this.setState({searchValue: e.target.value});
  }

  handleSubmit(e) {
    // Default behavior for a form is to refresh page - we don't want that. We handle the change ourselves
    this.setState({newQuery: true});
    e.preventDefault();
  }

  getFilterString() {
    let result = "";
    let order = [];
    let mins = [];
    let maxes = [];
    let filters = [];
    let f2s = [];
    let order_by = [];
    if(this.state.currentFilters) {
        console.log("CURRENT FILTERS: ");
        console.log(this.state.currentFilters);
      Object.keys(this.state.currentFilters).forEach(key => {
        // add filter to list of filter keywords
        if (this.state.currentFilters[key].realKey != "order_by" && this.state.currentFilters[key].realKey != "order" ){
          filters.push(this.state.currentFilters[key].realKey);
        }
        
        if (this.state.currentFilters[key].type === "string") {
          f2s.push(this.state.currentFilters[key].value);
        } else if (this.state.currentFilters[key].type === "order_by") {
          let val = this.state.currentFilters[key].value

          switch (val) {
            case "In-State Tuition":
              order_by.push("in_state_tuition");
              break;
            case "Out-Of-State Tuition":
              order_by.push("out_of_state_tuition");
              break;
            case "Admission Rate":
              order_by.push("admission_rate");
              break;
            case "Population":
              order_by.push("undergrad_size");
              break;
            case "Financial Aid Given":
              order_by.push("median_original_aid");
              break;
            case "Graduation Rate":
              order_by.push("completion_rate");
              break;
            case "Time":
              order_by.push("start_time");
              break;
            case "Admission Cost":
              order_by.push("cost");
              break;
            case "City Population":
              order_by.push("population");
              break;
            case "Monthly Cost of Living":
              order_by.push("monthly_cost_of_living");
              break;
            case "Average Monthly Salary":
              order_by.push("avg_monthly_salary");
              break;
            default:
              break;
          }
        }  else if (this.state.currentFilters[key].type === "order") {
            order.push(this.state.currentFilters[key].value.toLowerCase())
        } else {
          let tokens = this.state.currentFilters[key].value.split('-');
          if(tokens.length > 1) {
            // has min and max
            mins.push(tokens[0].trim());
            maxes.push(tokens[1].trim());
          } else {
            // either a max or a min
            if(tokens[0].includes('<')) {
              // we have max
              mins.push("0");
              maxes.push(tokens[0].split(' ')[1]);
            } else if(tokens[0].includes('>')) {
              // we have min
              mins.push(tokens[0].split(' ')[1]);
              maxes.push("_");
            }
          }
        }


        
      });
      // now we loop through our filters, mins, and maxes and add them to result
      result += "&filters=";
      result += filters.join(',');

      result += "&mins=";
      result += mins.join(',');

      result += "&maxes=";
      result += maxes.join(',');

      result += "&f2="
      result += f2s.join(',')

      result += "&order_by="
      result += order_by.join(',')

      result += "&order="
      result += order.join(',')

    }

    return result;
  }

  hasNext() {
    if (this.state.items.length < 15)
      return false;
    else return true;
  }

  render() {

    if (this.state.newQuery === true) {
      console.log(`redirecting to: ${this.props.global ? "search" : this.state.modelType}?${this.state.searchValue && "token=" + this.state.searchValue}` + this.getFilterString())
      return <Redirect to={`${this.props.global ? "search" : this.state.modelType}?${this.state.searchValue && "token=" + this.state.searchValue}` + this.getFilterString()} />
    }

    if(this.state.type === "team") {
      return(
        this.state.items ? <div className="container">
          <div className="row">
            {this.state.items.map( (item, index) => {
                return <TeamCard key={index} item={item} />
            })}
          </div>
        </div> : <Loading/>
      );
    } else {
      return (
        <div className={this.state.type === "grid" ? "container" : "container text-left"}>
          <h1 style={{marginBottom: "60px"}}>{this.state.title}</h1>
          <form className="input-group mb-4" onSubmit={this.handleSubmit} >
            <div className="input-group mb-4">
              <input type="search" value={this.state.searchValue} placeholder="What are you searching for?" aria-describedby="button-addon5" className="form-control" onChange={this.handleInput} />
              <div className="input-group-append">
                <button id="button-addon5" type="submit" className="btn search-btn" value={"submit"}>
                  <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img"
                       xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                       className="svg-inline--fa fa-search fa-w-16">
                    <path fill="#FFF"
                          d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"/>
                  </svg>
                </button>
              </div>
            </div>
            { (this.props.filters && this.state.items) && <FilterList updateFilters={this.updateFilters} filters={this.props.filters} items={this.state.items}/>}
          </form>
          {this.state.items ?
              <Fragment>
                  <div className="row">
                    {this.state.items.length > 0 ? this.state.items.map( (item, index) => {
                      return <GridItem key={index} item={item} modelType={this.props.modelType}/>
                    }) : <h1 className="col">No results</h1>}
                    {this.state.items.length > 0 && <Pagination pageNumber={this.state.pageNumber} query={this.state.lastQuery} nextPage={this.hasNext()}/>}
                  </div>
              </Fragment>
              : <Loading/>}
        </div>
      );
    }
  }
}

export default ItemList;
