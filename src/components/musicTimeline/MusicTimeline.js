import React from 'react';
import './MusicTimeline.css';
import * as d3 from 'd3';

const musicData = [3, 11, 17, 18, 6, 4, 7, 3, 0, 39, 55, 31];
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

class MusicTimeline extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      width: props.width || null,
      height: props.height || null
    };
    this.updateVisual = this.updateVisual.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.updateVisual();
  }

  componentDidUpdate(){
    this.updateVisual();
  }

  getData(){
    
  }

  updateVisual(){
    const node = this.node

    var musicObjects = [];

    for(var i = 0; i < months.length; i++){
      var obj = {
        month:months[i],
        value:musicData[i]
      }
      musicObjects.push(obj)
    }

    //Get college data
    //Spacing variables
    var paddingLeft = 60;
    var paddingBottom = 150;
    var paddingTop = 100;

    const dataMax = d3.max(musicData);
    
    const xScale = d3.scalePoint()
      .domain(months)
      .range([paddingLeft, this.state.width])
      .padding(0.1);
    
    const yScale = d3.scaleLinear()
      .domain([0, dataMax])
      .range([this.state.height - paddingBottom, paddingTop]);
   
    const yAxis = d3.axisLeft(yScale);

    const xAxis = d3.axisBottom(xScale);
    console.log(musicObjects);
    const lineGen = d3.line()
      .x(function (d) { return xScale(d.month)})
      .y(function (d) { return yScale(d.value)});

    //Add y axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'yAxis')
      .attr('transform', 'translate(' + paddingLeft + ',0)')
      .call(yAxis);

    //Add x axis to image
    d3.select(node)
      .append('g')
      .attr('id', 'xAxis')
      .attr('transform', 'translate(0, ' + (this.state.height - paddingBottom) + ')')
      .call(xAxis);
    
    d3.select(node)
      .selectAll('#xAxis')
      .selectAll('.tick')
      .selectAll('text')
      .attr('dx', -10)
      .attr('dy', 0)
      .attr('text-anchor', 'end')
      .attr('transform', 'rotate(315)')
    
    d3.select(node)
      .append('text')
      .attr('x', 30)
      .attr('y', yScale(dataMax / 2))
      .attr('text-anchor', 'middle')
      .attr('transform', 'rotate(270, 30,' + yScale(dataMax/2) + ')')
      .text('Number of Events');
    
    d3.select(node)
      .append('text')
      .attr('x', (this.state.width - paddingLeft) / 2)
      .attr('y', (this.state.height - 30))
      .attr('text-anchor', 'middle')
      .text('Month');
   
    d3.select(node)
      .selectAll('#datum')
      .data(musicData)
      .enter()
      .append('g')
      .attr('id', 'datum')
      .append('circle')
      .attr('id', 'point')
      .style('fill', '#007bff')
      .attr('index', (d, i) => i)
      .attr('cx', (d,i) => xScale(months[i]))
      .attr('cy', (d,i) => yScale(d))
      .attr('r', 5);

  }

  render() {
    return(
      <svg ref={node => this.node = node}
      width={this.state.width} height={this.state.height}>
      </svg>
    );
  }

}

export default MusicTimeline; 
