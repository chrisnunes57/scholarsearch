// This file houses util functions, to be used elsewhere inthe project
// It is called 'index.js' to make it easier to implicitly import in components

export function setCacheItem(slug, obj) {
  if(window.localStorage.getItem(slug) === null) {
    let stringData = JSON.stringify(obj)
    window.localStorage.setItem(slug, stringData);
  }
}

export function getCacheItem(slug) {
  return JSON.parse(window.localStorage.getItem(slug));
}

export function getLocation(item) {
  return (item.city && typeof item.city !== "number" ? item.city + (item.state ? ', ' : "") : "") + (item.state ? item.state : "");
}

export function getSlug(name) {
  return name ? name.split(' ').join('-').toLowerCase() : "Error: getSlug was passed a null String";
}

function formatDate(date) {
  let x = date.split("-");
  let months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  let day;
  if (x[2] === "01") {
    day = "1st";
  }
  else if (x[2] === "02") {
    day = "2nd";
  }
  else if (x[2] === "03") {
    day = "3rd";
  }
  else {
    day = "" + parseInt(x[2]) + "th";
  }
  return "" + months[parseInt(x[1]) - 1] + " " + day + ", " + x[0]
 
}

export function formatItem(item, type, matches) {

  item.type = type;

  if(type === "cities") {
    item.importantProperties = {
      "Population": item.population || "N/A",
      "Average Monthly Salary": item.avg_monthly_salary || "N/A",
      "Monthly Cost of Living": item.monthly_cost_of_living || "N/A",
      "Public Transportation": item.transportation || "N/A",
      "Matches" : matches 
    }
  } else if(type === "colleges") {
      item.importantProperties = {
        "Population": item.undergrad_size,
        "Graduate Salary": item.graduate_earnings,
        "Popular Major": item.popular_major || "Unknown",
        "Financial Aid Given": item.median_original_aid,
        "In-State Tuition": item.in_state_tuition,
        "Out-of-State Tuition": item.out_of_state_tuition
      }

      item.additionalProperties = {
        "Admission Rate": (item.admission_rate * 100).toFixed(2) +  "%",
        "Graduation Rate": (item.completion_rate * 100).toFixed(2) +  "%",
        "University Website": item.url,
        "Undergrad Size": item.undergrad_size,
        "Matches" : matches 
      }
    } else if(type === "events") {
    item.importantProperties = {
      "Venue": item.venue || "N/A",
      "Event Type": item.event_type || "N/A",
      "Date": formatDate(item.start_date) || "N/A",
      "Time": item.start_time || "N/A",
      "Admission Cost": item.cost || "Free",
      "Age Restriction": item.age_restriction || "None",
      "Matches" : matches 
    }

    item.additionalProperties = {
      "Description": item.description || "None"
    }
  } else if(type === "weather") {
    item.importantProperties = {
      "Condition": item.state,
      "Date": formatDate(item.date) || "N/A",
      "Time": item.time || "N/A",
      "High": item.temp_max || "N/A",
      "Low": item.temp_min || "N/A",
      "Relative Humidity": item.humidity + "%" || "N/A"
    }
  }

  return item;
}
