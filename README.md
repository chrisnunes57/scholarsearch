Members: 
    
*  Chris Nunes (CJN758)
    * Estimated Completion Time for Phase 4 (ECT): 15
    * Actual Completion Time for Phase 4 (ACT): 20
*  Hadi Channaa (HAC857)
    * ECT: 60
    * ACT: 35
*  Gautham Gujjula (GG28369)
    * ECT: 30
    * ACT: 40
*  Rushil Dandamudi (RCD948)
    * ECT: 30
    * ACT: 40
*  Rishi Iyer (RI2744)
    * ECT: 30
    * ACT: 40

Latest Git SHA for Grading: f3f22c8e59d5c0e24e017fd4c23a65e6871c0815

Project Leader for Phase 4: Rushil & Rishi (Co-Leaders) 

[Pipelines](https://gitlab.com/chrisnunes57/scholarsearch/pipelines)

[Website: scholarsearch.net](https://scholarsearch.net/)

Comments:

## Running the Project with Docker
```
make pull # gets the docker image from docker hub

make rebuild # rebuilds docker image using Dockerfile and docker-compose.yaml

make push # updates docker image in docker hub

make react # runs the docker image for frontend but opens up an interactive shell for any other npm commands
    example next command: make frontendbuild # runs npm run build to build a static dir for uploading to GCP and copies it to host directory

make flask # same as above except for backend

OR

make run # runs both docker images (front and back end) with default behavior (default is to run the front end with npm start and backend with python __init__.py; exit with ^C)


make frontendbuild # creates the static build directory used for deployment
```

## Pushing new static site from GCP Bucket
https://medium.com/google-cloud/how-to-deploy-a-static-react-site-to-google-cloud-platform-55ff0bd0f509
- Upload build folder
- Open GCP console in browser
- If you don't have an existing directory, create one like ```mkdir my-app```
- To sync build folder to the folder you just created, run ```gsutil rsync -r gs://scholarsearch-static ./my-app```
- Move into your folder ```cd my-app```
- Publish app with ```gcloud deploy app```

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This project allows automatic deployment to google cloud.