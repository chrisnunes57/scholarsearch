from math import radians, cos, sin, asin, sqrt
from db_client import db
from models import *

cities = City.query.all()

i = 1
n = str(len(cities))
for city in cities:
    print(city.name + " (" + str(i) + "/" + n + ")")
    i += 1
    events = city.events
    colleges = city.colleges.all()
    for event in events:
        event.colleges = colleges
        db.session.add(event)
        db.session.commit()
