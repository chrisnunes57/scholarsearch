import time
import json
import requests
#import __init__
from db_client import db
from models import *
from sqlalchemy.exc import DataError
from simplejson.errors import JSONDecodeError

def parsedata(data):
  weather_list = [];
  weather_to_parse = None;
  try:
    weather_to_parse = data["list"]
  except KeyError:
    return weather_list
  for weather in weather_to_parse:
    w = WeatherEntry()
    try:
      w.temp = weather["main"]["temp"]
    except KeyError:
      pass
    try:
      w.temp_min = weather["main"]["temp_min"]
    except KeyError:
      pass
    try:
      w.temp_max = weather["main"]["temp_max"]
    except KeyError:
      pass
    try:
      w.humidity = weather["main"]["humidity"]
    except KeyError:
      pass
    try:
      w.state = weather["weather"][0]["main"]
    except KeyError:
      pass
    try:
      w.icon_url = "http://openweathermap.org/img/wn/" + weather["weather"][0]["icon"] + "@2x.png"
    except KeyError:
      pass
    try:
      w.date = weather["dt_txt"][0:10]
    except KeyError:
      pass
    try:
      w.time = weather["dt_txt"][11:]
    except KeyError:
      pass
    weather_list.append(w)
  return weather_list

api_key = "&APPID=d46c1d11ca0225dc17dd6bbc33d441b9"
city_fields = ["&q=", ",us"]
api_url = "http://api.openweathermap.org/data/2.5/forecast?units=imperial"

cities = City.query.all()
failed_cities = list()

i = 1
n = str(len(cities))
start_time = time.clock_gettime(time.CLOCK_MONOTONIC)
for city in cities:
  print(city.name + " (" + str(i) + "/" + n + ")")
  i += 1
  request_url = api_url + city_fields[0] + city.name + city_fields[1] + api_key
  #print(request_url)
  data = None;
  try:
    data = requests.get(request_url).json()
  except JSONDecodeError:
    data = dict();
  #print(data)
  weather_list = parsedata(data)
  #print(weather_list)
  print(len(weather_list))
  try:
    weather = Weather()
    weather.city = city
    weather.weather_entries.extend(weather_list)
    for entry in weather_list:
      db.session.add(entry)
    db.session.add(weather)
    db.session.add(city)
    db.session.commit()
  except DataError as e:
      failed_cities.append(city.name)
      print(e)
      db.session.rollback()
  if(i % 60 == 0):
    time_diff = time.clock_gettime(time.CLOCK_MONOTONIC) - start_time
    if(time_diff < 60):
      time.sleep(60 - time_diff)
    start_time = time.clock_gettime(time.CLOCK_MONOTONIC)

print(failed_cities)
