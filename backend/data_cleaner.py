#Deprecated, we use json_schemas now 
#Cleans data for front-end
from flask import jsonify
from models import *

def clean_college(college):
    important_props = dict()
    important_props["Population"] = college.undergrad_size
    important_props["Average Graduate Salary"] = college.graduate_earnings
    important_props[
        "Most Popular Major"
    ] = college.majors.order_by(Major.percent_of_undergrad.desc()).first()  # TODO: find popular major
    important_props["Average Financial Aid Given"] = college.median_original_aid
    important_props[
        "Average Tuition"
    ] = college.in_state_tuition  # TODO: specify tuition

    college_dict = college.__dict__
    college_dict["importantProperties"] = important_props
    return jsonify(college_dict)

def clean_city(city):
    important_props = dict()
    important_props["Population"] = city.population
    important_props["Average Cost of Living"] = city.costs  # TODO: find average
    important_props["Weather"] = city.population  # TODO: get weather
    important_props["Public Transportation System"] = city.population  # TODO: get PTS
    important_props["Biggest Industry"] = city.population  # TODO: get Biggest industry

    city_dict = city.__dict__
    city_dict["importantProperties"] = important_props
    return jsonify(city_dict)


def clean_cost(cost):
    pass
