from flask_sqlalchemy import SQLAlchemy


#to access database:
#> ./cloud_sql_proxy -instances=scholarsearch-254800:us-central1:scholarsearch-phase2-db1=tcp:3306 -credential_file=scholarsearch-254800-8236626313f5.json

#to access mysql client:
#> mysql -u root -p --host 127.0.0.1

#Initialize Database Client
db = SQLAlchemy()
