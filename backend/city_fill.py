import json
import requests
from db_client import db
from models import *
from sqlalchemy import desc
import wikipedia 

states = {
    "AL": "01",
    "AK": "02",
    "AZ": "04",
    "AR": "05",
    "CA": "06",
    "CO": "08",
    "CT": "09",
    "DE": "10",
    "DC": "11",
    "FL": "12",
    "GA": "13",
    "HI": "15",
    "ID": "16",
    "IL": "17",
    "IN": "18",
    "IA": "19",
    "KS": "20",
    "KY": "21",
    "LA": "22",
    "ME": "23",
    "MD": "24",
    "MA": "25",
    "MI": "26",
    "MN": "27",
    "MS": "28",
    "MO": "29",
    "MT": "30",
    "NE": "31",
    "NV": "32",
    "NH": "33",
    "NJ": "34",
    "NM": "35",
    "NY": "36",
    "NC": "37",
    "ND": "38",
    "OH": "39",
    "OK": "40",
    "OR": "41",
    "PA": "42",
    "RI": "44",
    "SC": "45",
    "SD": "46",
    "TN": "47",
    "TX": "48",
    "UT": "49",
    "VT": "50",
    "VA": "51",
    "WA": "53",
    "WV": "54",
    "WI": "55",
    "WY": "56",
    "PR": "72"
}



def get_population(cities, data):
    for city in cities:
        if city.population is None:
            print("City = " + city.name) 
            pops = list(filter(lambda x: city.name + " city" in x[0] or 
                                         city.name + " town" in x[0] or 
                                         city.name + " village" in x[0] or
                                         city.name + " borough" in x[0] or
                                         city.name + " municipality" in x[0], data))
            pop = 0
            for x in pops:
                pop += int(x[1])
            print(pops)
            print("total population: ", pop)
            city.population = pop;
            db.session.commit()


def parse_costs(city):
    monthly_cost = 0.0
    for cost in city.costs:
        if cost.category is None:
            name = cost.name.split(", ")
            cost.category = name[-1]
            if "month" in cost.name.lower() or cost.category in ['Restaurants', 'Markets', 'Sports and Leisure'] :
                if cost.name == "Average Monthly Net Salary (After Tax), Salaries And Financing":
                    city.avg_monthly_salary = cost.amount
                elif cost.name not in ["Apartment (1 bedroom) Outside of Centre, Rent Per Month", "Apartment (3 bedrooms) in City Centre, Rent Per Month", "Apartment (3 bedrooms) Outside of Centre, Rent Per Month"]:
                    monthly_cost += cost.amount
            cost.name = name[0]
            db.session.commit()
    if monthly_cost > 0.0:
        city.monthly_cost_of_living = monthly_cost 
        db.session.commit()
    if city.avg_monthly_salary is None:
        salaries = [x[0] for x in City.query.filter_by(name="New York").first().colleges.with_entities(College.graduate_earnings).all()]
        salaries = list(filter(lambda x: x is not None, salaries))
        city.avg_monthly_salary = sum(salaries) // len(salaries)
        db.session.commit()

for state in states.keys():
    cities = City.query.filter(City.state == state).order_by(desc(City.id)).all()
    print("State = " + state)
    try:
        data = requests.get("https://api.census.gov/data/2018/pep/population?get=GEONAME,POP&for=place:*&in=state:" + states[state]).json()
    except Exception as e:
        print(e)
    else:
        get_population(cities, data)
    for city in cities:
        if city.img_src is None:
            try:
                data = requests.get("https://www.googleapis.com/customsearch/v1?key=AIzaSyCQIm7bpIJv2I7f9IVMc9LAntCeiBauOJk&num=1&searchType=image&imgSize=large&cx=001695048695817903099:gy2bjrw7dnj&q=" + city.name + " " + city.state + " downtown").json()
                link = data["items"][0]["link"]
            except Exception as e:
                print(e)
            else:
                if len(link) <= 300:
                    city.img_src = link
                else:
                    try:
                        page = wikipedia.page(city.name + ", " + city.state)
                        city.img_src = page.images[0]
                    except Exception as e:
                        print(e)
                db.session.commit()
        parse_costs(city) 

cities = City.query.filter(City.population == 0).all()
for city in cities:
    print(city.name + ", " + city.state)
    name = city.name
    if "Saint " in name:
        name= "St. " + name[6:-1]
    try:
        data = requests.get("https://public.opendatasoft.com/api/records/1.0/search/?dataset=worldcitiespop&sort=-population&facet=country&refine.country=us&refine.accentcity="+name.lower()+"&refine.region="+city.state).json()
        city.population = data["records"][0]["fields"]["population"]
    except Exception as e:
        print(e)
    else:
        db.session.commit()
    data = requests.get(
        "https://www.numbeo.com/api/city_prices?api_key=qso12dvfirx1ii&query="
        + college_city.name
    ).json()
    try:
        prices = data["prices"]
    except KeyError:
        pass
    else:
        for price in prices:
            try:
                cost = Cost(
                    name=price["item_name"],
                    amount=price["average_price"],
                    city=college_city,
                )
            except KeyError:
                pass
            db.session.add(cost)
            db.session.commit()




