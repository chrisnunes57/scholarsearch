import unittest
import config
import db_client
from flask_sqlalchemy import SQLAlchemy
import json_schemas
import flask_marshmallow
import marshmallow_sqlalchemy
import models
from __init__ import app
import sqlalchemy
import routes

class RoutesTest(unittest.TestCase):
  def setUp(self):
    self.app = app.test_client()

  def tearDown(self):
    pass
  
  def test1(self):
    def inner_test():
      return 1
    q = routes.general_query(inner_test)
    self.assertEqual(q.__name__, "inner_test")
  
  def test2(self):
    pass

  def test3(self):
    pass

  def test4(self):
    f = open("./route_tests/test4_out", "r")
    expected = f.read().strip()
    f.close()
    response = self.app.get("/")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data.decode("utf-8").strip(), expected) 

  def test5(self):
    f = open("./route_tests/test5_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/colleges/query", query_string={"id" : 331})
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test6(self):
    f = open("./route_tests/test6_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/events/query", query_string={"id" : 1})
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test7(self):
    f = open("./route_tests/test7_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/cities/query", query_string={"id" : 1})
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 
  
  def test8(self):
    f = open("./route_tests/test8_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/colleges/harvard%20university")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test9(self):
    f = open("./route_tests/test9_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/colleges")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test10(self):
    f = open("./route_tests/test10_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/colleges/page=1")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test11(self):
    f = open("./route_tests/test11_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/events/Post%20Malone%20-%20Runaway%20Tour")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test12(self):
    f = open("./route_tests/test12_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/events")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test13(self):
    f = open("./route_tests/test13_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/events/page=1")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test14(self):
    f = open("./route_tests/test14_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/cities/Austin")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test15(self):
    f = open("./route_tests/test15_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/cities")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected) 

  def test16(self):
    f = open("./route_tests/test16_out", "r")
    expected = ''.join(f.read().split())
    f.close()
    response = self.app.get("/cities/page=1")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(''.join(response.data.decode("utf-8").split()), expected)


class ModelsTest(unittest.TestCase):
  def test1(self):
    self.assertEqual(type(models.city_event_table), sqlalchemy.sql.schema.Table)
    self.assertEqual(type(models.college_event_table), sqlalchemy.sql.schema.Table)

  def test2(self):
    m = models.College()
    self.assertEqual(type(m), models.College)
    self.assertTrue(issubclass(type(m), models.db.Model))
    self.assertEqual(m.__repr__(), '<College None>')

  def test3(self):
    m = models.Weather()
    self.assertEqual(type(m), models.Weather)
    self.assertTrue(issubclass(type(m), models.db.Model))
    self.assertEqual(m.__repr__(), '<Weather None>')

  def test4(self):
    m = models.WeatherEntry()
    self.assertEqual(type(m), models.WeatherEntry)
    self.assertTrue(issubclass(type(m), models.db.Model))
    self.assertEqual(m.__repr__(), '<WeatherEntry None>')
  
  def test5(self):
    m = models.Major()
    self.assertEqual(type(m), models.Major)
    self.assertTrue(issubclass(type(m), models.db.Model))
    self.assertEqual(m.__repr__(), '<Major None>')
  
  def test6(self):
    m = models.City()
    self.assertEqual(type(m), models.City)
    self.assertTrue(issubclass(type(m), models.db.Model))
    self.assertEqual(m.__repr__(), '<City None>')
  
  def test7(self):
    m = models.Cost()
    self.assertEqual(type(m), models.Cost)
    self.assertTrue(issubclass(type(m), models.db.Model))
    self.assertEqual(m.__repr__(), '<Cost None>')
  
  def test8(self):
    m = models.Event()
    self.assertEqual(type(m), models.Event)
    self.assertTrue(issubclass(type(m), models.db.Model))
    self.assertEqual(m.__repr__(), '<Event None>')

class JsonSchemasTest(unittest.TestCase):
  def test1(self):
    s = json_schemas.MajorSchema()
    m = s.Meta()
    self.assertEqual(type(s), json_schemas.MajorSchema)
    self.assertEqual(s.Meta, json_schemas.MajorSchema.Meta)
    self.assertEqual(m.model, models.Major)

  def test2(self):
    s = json_schemas.CollegeSchema()
    m = s.Meta()
    self.assertEqual(type(s), json_schemas.CollegeSchema)
    self.assertEqual(s.Meta, json_schemas.CollegeSchema.Meta)
    self.assertEqual(m.model, models.College)

  def test3(self):
    s = json_schemas.CostSchema()
    m = s.Meta()
    self.assertEqual(type(s), json_schemas.CostSchema)
    self.assertEqual(s.Meta, json_schemas.CostSchema.Meta)
    self.assertEqual(m.model, models.Cost)

  def test4(self):
    s = json_schemas.EventSchema()
    m = s.Meta()
    self.assertEqual(type(s), json_schemas.EventSchema)
    self.assertEqual(s.Meta, json_schemas.EventSchema.Meta)
    self.assertEqual(m.model, models.Event)

  def test5(self):
    s = json_schemas.CitySchema()
    m = s.Meta()
    self.assertEqual(type(s), json_schemas.CitySchema)
    self.assertEqual(s.Meta, json_schemas.CitySchema.Meta)
    self.assertEqual(m.model, models.City)

class ConfigTest(unittest.TestCase):
  def test1(self):
    c = config.BaseConfig()
    self.assertEqual(c.DEBUG, False)
    self.assertEqual(c.TESTING, False)

  def test2(self):
    c = config.DevelopmentConfig()
    self.assertEqual(c.DEBUG, True)
    self.assertEqual(c.TESTING, True)
  
  def test3(self):
    c = config.TestingConfig()
    self.assertEqual(c.DEBUG, False)
    self.assertEqual(c.TESTING, True)
  
  def test4(self):
    c = config.DeploymentConfig()
    self.assertEqual(c.DEBUG, False)
    self.assertEqual(c.TESTING, False)

class DBClientTest(unittest.TestCase):
  def test1(self):
    self.assertEqual(type(db_client.db), SQLAlchemy)

if __name__ == "__main__":
  unittest.main()
