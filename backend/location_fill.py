import json
import requests
from db_client import db
from models import *
from math import radians, cos, sin, asin, sqrt

fields = "location.lat,location.lon"
school_api_key = "&api_key=Sxf71UYQVKi8DiEW4Gp2HuQk4DBeTBEoXc8kMJsc"
school_api_url = "https://api.data.gov/ed/collegescorecard/v1/schools"
school_field = "?id="


#def add_coordinates(providing_college, entry):
    


colleges = College.query.filter(College.id > 1554)
for college in colleges:
    city = college.city
    if city.longitude is None and city.latitude is None:
        request = (
            school_api_url
            + school_field
            + str(college.api_id)
            + "&_fields="
            + fields
            + school_api_key
        )
        try:
            data = requests.get(request).json()
        except Exception as e:
            print(e)
        else:
            city.longitude =  data["results"][0]["location.lon"]
            city.latitude = data["results"][0]["location.lat"]
            db.session.commit()
            #print(request)
            #print(json.dumps(data, indent=4))
