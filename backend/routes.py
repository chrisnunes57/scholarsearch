from flask import request, jsonify, Blueprint, redirect, url_for
from models import *
from json_schemas import *
from data_cleaner import clean_college, clean_city
import traceback

routes = Blueprint('routes', __name__, template_folder='templates')

# allows for unpacking into general queries
def general_query(f):
    def q():
        request.parameter_storage_class = dict
        return f()
    q.__name__ = f.__name__
    return q

def all_query_wrapper(f):
    def wrapper(*args, **kwargs):
        try:
            query, schema, *extras = f(*args, **kwargs)
            return jsonify(data=schema.dump(query), extras=extras), 200
        except Exception as e:
            traceback.print_exc()
            return f'An Error Occured: {e}', 500
    wrapper.__name__ = f.__name__
    return wrapper


#for searching through DB

ITEMS_PER_PAGE = 15
IGNORE_LIMIT = False

def search_query(f):
    def q(*args, **kwargs):
        search_token = request.args.get('token', type=str)
        sort_direction = request.args.get('order', default='ascending', type=str)
        sort_key = request.args.get('order_by', type=str)
        other_filters = request.args.get('f2', type=str)
        filters, mins, maxes, other_filters = get_ranges(request)
        page_num = request.args.get('page', type=str)

        global IGNORE_LIMIT
        try:
            page_num = int(page_num)
            IGNORE_LIMIT = False
        except Exception:
            page_num = None
            IGNORE_LIMIT = False

        schema, queries, columns_lists, curr_class = f(*args, **kwargs)
        assert len(queries) == len(columns_lists)


        matches = []
        results = []
        search = ''
        if search_token is not None: # clean whitespace if there is a token
            search_token = search_token.strip()

        if search_token: # if token is not empty then make a strict search
            search = '%' + search_token + '%' # surround with any kind of character
        else:
            queries = [curr_class.query]

        queries = add_filters(curr_class, queries, filters, mins, maxes, other_filters)
        results, matches = get_search_results_for_queries(search, queries, columns_lists, curr_class.id)

        if len(results) == 0:
            return results, schema, {}

        results = sort_results(curr_class, results, sort_key, sort_direction)

        dict_matches = {}

        if search_token:
            dict_matches = {id_num: match for id_num, match in matches}

        if page_num and page_num > 0:
            results, dict_matches = get_page(results, dict_matches, page_num)

        return results, schema, dict_matches

    q.__name__ = f.__name__
    return q


def get_page(results, matches, page):
    start_index = (page - 1) * ITEMS_PER_PAGE
    # if page is too high, return nothing
    if start_index > len(results):
        return [], {}

    page_results = []
    page_matches = {}

    for i in range(start_index, start_index + ITEMS_PER_PAGE):
        if i >= len(results):
            return page_results, page_matches

        curr_result = results[i]

        page_results.append(curr_result)
        if curr_result.id in matches:
            page_matches[curr_result.id] = matches[curr_result.id]

    return page_results, page_matches

def normalize_length(arr, norm):
    if arr is None:
        return [None] * norm
    if len(arr) < norm:
        arr = arr + [None] * (norm - len(arr))
    return arr

def is_col_num(col_type):
    return isinstance(col_type, db.Numeric) or isinstance(col_type, db.Integer)


def normalize_length(arr, types, num_type=True):
    arr_index = 0
    new_arr = []
    for col_type in types:
        if arr_index < len(arr):
            arr_item = arr[arr_index]
        else:
            arr_item = None

        if num_type and is_col_num(col_type) or not num_type and isinstance(col_type, db.String):
            new_arr.append(arr_item)
            arr_index += 1
        else:
            new_arr.append(None)
    return new_arr

def get_column(class_name, col_name):
    return class_name.__table__.c[col_name]

def add_filters(curr_class, queries, filters, mins, maxes, other_filters=None):
    norm = len(filters)
    col_types = [get_column(curr_class, f).type for f in filters]

    mins = normalize_length(mins, col_types)
    maxes = normalize_length(maxes, col_types)
    other_filters = normalize_length(other_filters, col_types, False)

    for f, _min, _max, other_filter in zip(filters, mins, maxes, other_filters):
        try:
            col = get_column(curr_class, f)
        except Exception:
            return queries

        curr_type = col.type
        if other_filter and isinstance(curr_type, db.String):
            queries = [query.filter(col.isnot(None)).filter(col.ilike(other_filter)) for query in queries]
        elif isinstance(curr_type, db.Numeric) or isinstance(curr_type, db.Integer):
            queries = [query.filter(col.between(_min, _max)) for query in queries]
    return queries

def compare_result(result, sort_key):
    value = result.__dict__.get(sort_key, False)
    if value is None:
        value = False
    return value

def sort_results(curr_class, results, sort_key, sort_direction='ascending'):
    rev = False
    if sort_direction == 'descending':
        rev = True

    if sort_key is None:
        print("sort key is None .. not sorting")
        return results

    print(f'results: {results}')
    for i in range(len(results)):
        if results[i].__dict__ is None:
            print(i)
    if isinstance(get_column(curr_class, sort_key).type, db.String):
        return sorted(results, key = lambda result: result.__dict__.get(sort_key) if result.__dict__.get(sort_key) is not None else "", reverse=rev)
    return sorted(results, key = lambda result: result.__dict__.get(sort_key) if result.__dict__.get(sort_key) is not None else False, reverse=rev)





POSTMAN_URL = 'https://documenter.getpostman.com/view/9278671/SW7ey5YD?version=latest';
@routes.route("/", methods=["GET"])
def home():
    return (
        f"<h1>Welcome to the ScholarSearch API!</h1><a href='{POSTMAN_URL}' target='_blank'>Commands found here.</h2><a>"
    )


# SEARCHING


def clean_bounds(bound, max_bound=True):
    try:
        return float(bound)
    except ValueError:
        if max_bound:
            return float('99999999999999')
        return float('-99999999999999')

def get_arg_as_list(req, arg, def_type=str, fun=lambda x: x):
    req_arg = req.args.get(arg, type=def_type)
    if req_arg is None:
        return []
    args = req_arg.split(',')
    return [fun(arg.strip()) for arg in args]


def get_ranges(req):
    filters = get_arg_as_list(req, 'filters')
    mins = get_arg_as_list(req, 'mins', fun=clean_bounds)
    maxes = get_arg_as_list(req, 'maxes', fun=clean_bounds)
    other_filters = get_arg_as_list(req, 'f2')

    print(locals())

    return filters, mins, maxes, other_filters


SEARCH_LIMIT = 100

def get_search_results(search, sql_query, columns, main_id, kept_ids = set()):
    joined_results = []
    joined_matches = []
    for col in columns:
        matches = []
        if search: # if there is a search term, filter for it
            result = sql_query.filter(~main_id.in_(kept_ids)).filter(col.like(search))
            if not IGNORE_LIMIT:
                result = result.limit(SEARCH_LIMIT)
            matches = result.with_entities(main_id, col).all()
        else: # otherwise continue regular query 
            result = sql_query.filter(~main_id.in_(kept_ids))
            if not IGNORE_LIMIT:
                result = result.limit(SEARCH_LIMIT)

        joined_results.extend(result.all())
        joined_matches.extend(matches)

        cleaned_ids = {i for i, in result.with_entities(main_id).all()}
        kept_ids = kept_ids.union(cleaned_ids)

        if not IGNORE_LIMIT and len(joined_results) > SEARCH_LIMIT:
            return joined_results, joined_matches, kept_ids

    return joined_results, joined_matches, kept_ids

def get_search_results_for_queries(search, sql_queries, columns_per_query, main_id, kept_ids = set()):
    all_results = []
    all_matches = []

    for query, columns in zip(sql_queries, columns_per_query):
        joined_results, joined_matches, kept_ids = get_search_results(search, query, columns, main_id, kept_ids)
        assert not search or len(joined_results) == len(joined_matches)

        if len(joined_results) > 0:
            all_results.extend(joined_results)
            all_matches.extend(joined_matches)

        if not IGNORE_LIMIT and len(all_results) > SEARCH_LIMIT:
            return all_results, all_matches


    return all_results, all_matches

@routes.route("/search/colleges", methods=["GET"])
@all_query_wrapper
@search_query
def college_search():
    nearby_cols = [College.name]
    city_cols = [City.name]

    queries = [College.query, College.query.join(City, College.city_id == City.id)]
    college_schema = CollegeSchema(many=True)
    return college_schema, queries, [nearby_cols,  city_cols], College


@routes.route("/search/cities", methods=["GET"])
@all_query_wrapper
@search_query
def city_search():
    nearby_cols = [City.name, City.state]

    queries = [City.query]
    city_schema = CitySchema(many=True)
    return city_schema, queries, [nearby_cols], City

@routes.route("/search/events", methods=["GET"])
@all_query_wrapper
@search_query
def event_search():
    nearby_cols = [Event.name, Event.event_type, Event.venue, Event.address, Event.description, Event.organizer, Event.age_restriction]

    queries = [Event.query]
    event_schema = EventSchema(many=True)
    return event_schema, queries, [nearby_cols], Event

#for grabbing singular instances

@routes.route("/colleges/query", methods=["GET"])
@general_query
@all_query_wrapper
def general_college_query():
    college_schema = CollegeSchema(many=True)
    page = -1
    print(request.args)
    if 'page' in request.args:
        page = int(request.args['page'])
        del request.args['page']
    colleges = College.query.filter_by(**request.args)

    if (page > 0):
        items = colleges.paginate(page=page, per_page=15, error_out=False).items
        return items, college_schema
    return colleges, college_schema

@routes.route("/events/query", methods=["GET"])
@general_query
@all_query_wrapper
def general_event_query():
    event_schema = EventSchema(many=True)
    page = -1
    print(request.args)
    if 'page' in request.args:
        page = int(request.args['page'])
        del request.args['page']
    events = Event.query.filter_by(**request.args)

    if (page > 0):
        items = events.paginate(page=page, per_page=15, error_out=False).items
        return items, event_schema
    return events, event_schema

@routes.route("/cities/query", methods=["GET"])
@general_query
@all_query_wrapper
def general_city_query():
    city_schema = CitySchema(many=True)
    page = -1
    print(request.args)
    if 'page' in request.args:
        page = int(request.args['page'])
        del request.args['page']
    cities = City.query.filter_by(**request.args)

    if (page > 0):
        items = cities.paginate(page=page, per_page=15, error_out=False).items
        return items, city_schema
    return cities, city_schema


# Colleges


@routes.route("/colleges/<string:college_name>", methods=["GET"])
@all_query_wrapper
def get_college(college_name):
    # query from db: get college with name = college_name
    college = College.query.filter_by(name=college_name).first()
    college_schema = CollegeSchema()
    return college, college_schema

@routes.route("/colleges", methods=["GET"])
@all_query_wrapper
def get_colleges():
    colleges = College.query.paginate(page=1, per_page=15, error_out=False).items
    college_schema = CollegeSchema(many=True)
    return colleges, college_schema

@routes.route("/colleges/page=<int:page_number>", methods=["GET"])
@all_query_wrapper
def get_colleges_paginated(page_number):
    colleges = College.query.paginate(page=page_number, per_page=15, error_out=False).items
    college_schema = CollegeSchema(many=True)
    return colleges, college_schema


# Events

@routes.route('/events/<string:event_name>', methods=['GET'])
@all_query_wrapper
def get_event(event_name):
    # query from db: get Event with name = event_name
    event = Event.query.filter_by(name=event_name).first()
    event_schema = EventSchema()
    return event, event_schema


@routes.route('/events', methods=['GET'])
@all_query_wrapper
def get_events():
    events = Event.query.paginate(page=1, per_page=15, error_out=False).items
    event_schema = EventSchema(many=True)
    return events, event_schema

@routes.route('/events/page=<int:page_number>', methods=['GET'])
@all_query_wrapper
def get_events_paginated(page_number):
    events = Event.query.paginate(page=page_number, per_page=15, error_out=False).items
    event_schema = EventSchema(many=True)
    return events, event_schema

# Cities


@routes.route("/cities/<string:city_name>", methods=["GET"])
@all_query_wrapper
def get_city(city_name):
    # query from db: get city with name = city_name
    city = City.query.filter_by(name=city_name).first()
    city_schema = CitySchema()
    return city, city_schema



@routes.route("/cities", methods=["GET"])
@all_query_wrapper
def get_cities():
    cities = City.query.paginate(page=1, per_page=15, error_out=False).items
    city_schema = CitySchema(many=True)
    return cities, city_schema

@routes.route("/cities/page=<int:page_number>", methods=["GET"])
@all_query_wrapper
def get_cities_paginated(page_number):
    cities = City.query.paginate(page=page_number, per_page=15, error_out=False).items
    city_schema = CitySchema(many=True)
    return cities, city_schema
