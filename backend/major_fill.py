import json
import requests
from db_client import db
from models import *
from sqlalchemy import desc

# major_list = "latest.academics.program_percentage.agriculture,latest.academics.program_percentage.resources,latest.academics.program_percentage.architecture,latest.academics.program_percentage.ethnic_cultural_gender,latest.academics.program_percentage.communication,latest.academics.program_percentage.communications_technology,latest.academics.program_percentage.computer,latest.academics.program_percentage.personal_culinary,latest.academics.program_percentage.education,latest.academics.program_percentage.engineering,latest.academics.program_percentage.engineering_technology,latest.academics.program_percentage.language,latest.academics.program_percentage.family_consumer_science,latest.academics.program_percentage.legal,latest.academics.program_percentage.english,latest.academics.program_percentage.humanities,latest.academics.program_percentage.library,latest.academics.program_percentage.biological,latest.academics.program_percentage.mathematics,latest.academics.program_percentage.military,latest.academics.program_percentage.multidiscipline,latest.academics.program_percentage.parks_recreation_fitness,latest.academics.program_percentage.philosophy_religious,latest.academics.program_percentage.theology_religious_vocation,latest.academics.program_percentage.physical_science,latest.academics.program_percentage.science_technology,latest.academics.program_percentage.psychology,latest.academics.program_percentage.security_law_enforcement,latest.academics.program_percentage.public_administration_social_service,latest.academics.program_percentage.social_science,latest.academics.program_percentage.construction,latest.academics.program_percentage.mechanic_repair_technology,latest.academics.program_percentage.precision_production,latest.academics.program_percentage.transportation,latest.academics.program_percentage.visual_performing,latest.academics.program_percentage.health,latest.academics.program_percentage.business_marketing,latest.academics.program_percentage.history"
# school_api_key = "&api_key=Sxf71UYQVKi8DiEW4Gp2HuQk4DBeTBEoXc8kMJsc"
# school_api_url = "https://api.data.gov/ed/collegescorecard/v1/schools"
# school_field = "?id="


# def add_majors(providing_college, entry):
#     keys = major_list.split(",")
#     for key in keys:
#         substring = key.replace("latest.academics.program_percentage.", "")
#         if entry[key] > 0:
#             college_major = Major(
#                 name=substring.title(),
#                 college=providing_college,
#                 percent_of_undergrad=entry[key],
#             )
#             db.session.add(college_major)
#             db.session.commit()


colleges = College.query.filter(College.id > 977).all()

for college in colleges:
    major = college.majors.order_by(desc(Major.percent_of_undergrad)).first()
    if major is not None:
        college.popular_major = major.name
    data = requests.get("https://www.googleapis.com/customsearch/v1?key=AIzaSyCQIm7bpIJv2I7f9IVMc9LAntCeiBauOJk&num=1&searchType=image&imgSize=large&cx=001695048695817903099:gy2bjrw7dnj&q=" + college.name + " campus").json()
    try:
        college.img_src = data["items"][0]["link"]
    except Exception as e:
        print(e)
    db.session.commit()

# for college in colleges:
#     request = (
#         school_api_url
#         + school_field
#         + str(college.api_id)
#         + "&_fields="
#         + major_list
#         + school_api_key
#     )
#     try:
#         data = requests.get(request).json()
#     except Exception as e:
#         print(e)
#     else:
#         add_majors(college, data["results"][0])
#     # print(json.dumps(data, indent=4))
