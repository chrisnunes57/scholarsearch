import requests
import json

major_list = "latest.academics.program_percentage.agriculture,latest.academics.program_percentage.resources,latest.academics.program_percentage.architecture,latest.academics.program_percentage.ethnic_cultural_gender,latest.academics.program_percentage.communication,latest.academics.program_percentage.communications_technology,latest.academics.program_percentage.computer,latest.academics.program_percentage.personal_culinary,latest.academics.program_percentage.education,latest.academics.program_percentage.engineering,latest.academics.program_percentage.engineering_technology,latest.academics.program_percentage.language,latest.academics.program_percentage.family_consumer_science,latest.academics.program_percentage.legal,latest.academics.program_percentage.english,latest.academics.program_percentage.humanities,latest.academics.program_percentage.library,latest.academics.program_percentage.biological,latest.academics.program_percentage.mathematics,latest.academics.program_percentage.military,latest.academics.program_percentage.multidiscipline,latest.academics.program_percentage.parks_recreation_fitness,latest.academics.program_percentage.philosophy_religious,latest.academics.program_percentage.theology_religious_vocation,latest.academics.program_percentage.physical_science,latest.academics.program_percentage.science_technology,latest.academics.program_percentage.psychology,latest.academics.program_percentage.security_law_enforcement,latest.academics.program_percentage.public_administration_social_service,latest.academics.program_percentage.social_science,latest.academics.program_percentage.construction,latest.academics.program_percentage.mechanic_repair_technology,latest.academics.program_percentage.precision_production,latest.academics.program_percentage.transportation,latest.academics.program_percentage.visual_performing,latest.academics.program_percentage.health,latest.academics.program_percentage.business_marketing,latest.academics.program_percentage.history,"
school_api_key = "&api_key=xqiZf5YY3PIVtNHh9UnsTcZE7yCyxXsj3FtBf7uV"
school_api_url = "https://api.data.gov/ed/collegescorecard/v1/schools"
school_api_request = (
    "?_per_page=100&school.degrees_awarded.predominant=3&_sort=latest.student.size:desc"
)
general_fields = "school.name,school.city,school.state,school.zip,school.school_url,"
student_fields = "latest.student.size,latest.cost.tuition.in_state,latest.cost.tuition.out_of_state,latest.aid.loan_principal,"
other_fields = "latest.earnings.10_yrs_after_entry.working_not_enrolled.mean_earnings,latest.admissions.admission_rate.overall,latest.completion.completion_rate_4yr_150nt"
fields = "&_fields=" + general_fields + student_fields + major_list + other_fields
# #request = school_api_url + school_api_request + fields + school_api_key
# request = "https://app.ticketmaster.com/discovery/v2/events.json?countryCode=US&apikey=MbVP6JTuXFkctK66SkA0VgkQaNnWURCc"
# res1 = requests.get(request)
# data = res1.json()

data = requests.get("https://public.opendatasoft.com/api/records/1.0/search/?dataset=worldcitiespop&sort=-population&facet=country&refine.country=us&refine.accentcity=Van Nuys&refine.region=CA").json()
print(json.dumps(data, indent=4))
