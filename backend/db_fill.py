import json
import requests
import db_client

major_list = "latest.academics.program_percentage.agriculture,latest.academics.program_percentage.resources,latest.academics.program_percentage.architecture,latest.academics.program_percentage.ethnic_cultural_gender,latest.academics.program_percentage.communication,latest.academics.program_percentage.communications_technology,latest.academics.program_percentage.computer,latest.academics.program_percentage.personal_culinary,latest.academics.program_percentage.education,latest.academics.program_percentage.engineering,latest.academics.program_percentage.engineering_technology,latest.academics.program_percentage.language,latest.academics.program_percentage.family_consumer_science,latest.academics.program_percentage.legal,latest.academics.program_percentage.english,latest.academics.program_percentage.humanities,latest.academics.program_percentage.library,latest.academics.program_percentage.biological,latest.academics.program_percentage.mathematics,latest.academics.program_percentage.military,latest.academics.program_percentage.multidiscipline,latest.academics.program_percentage.parks_recreation_fitness,latest.academics.program_percentage.philosophy_religious,latest.academics.program_percentage.theology_religious_vocation,latest.academics.program_percentage.physical_science,latest.academics.program_percentage.science_technology,latest.academics.program_percentage.psychology,latest.academics.program_percentage.security_law_enforcement,latest.academics.program_percentage.public_administration_social_service,latest.academics.program_percentage.social_science,latest.academics.program_percentage.construction,latest.academics.program_percentage.mechanic_repair_technology,latest.academics.program_percentage.precision_production,latest.academics.program_percentage.transportation,latest.academics.program_percentage.visual_performing,latest.academics.program_percentage.health,latest.academics.program_percentage.business_marketing,latest.academics.program_percentage.history,"
school_api_key = "&api_key=xqiZf5YY3PIVtNHh9UnsTcZE7yCyxXsj3FtBf7uV"
school_api_url = "https://api.data.gov/ed/collegescorecard/v1/schools"
school_api_request = "?_per_page=100&school.degrees_awarded.predominant=3&_sort=latest.student.size:desc"
general_fields = "school.name,school.city,school.state,school.zip,school.school_url,"
student_fields = "latest.student.size,latest.cost.tuition.in_state,latest.cost.tuition.out_of_state,latest.aid.loan_principal,"
other_fields = "latest.earnings.10_yrs_after_entry.working_not_enrolled.mean_earnings,latest.admissions.admission_rate.overall,latest.completion.completion_rate_4yr_150nt"
fields = "&_fields=" + general_fields + student_fields + major_list + other_fields
request = school_api_url + school_api_request + fields + school_api_key
res1 = requests.get(request)

data = res1.json()
print(json.dumps(data, indent = 4))

for page in range(0, data["metadata"]["total"] // data["metadata"]["_per_page"]):
	response = requests.get(request + "&page=%d" % x)
	try:
		data = response.json()
	except Exception as e:
	 	print(e)
	else:
		fill_colleges(data)

def fill_colleges(data):
	for entry in data["results"]:
		college = College.query.filter_by(entry["school.name"]).first()
		if college is None:
			college_city = City.query.filter_by(name = entry["school.city"]).first()
			if college_city is None:
				college_city = create_city(entry["school.city"], entry["school.state"])
			college = College(name = entry["school.name"], undergrad_size= entry["latest.student.size"], city=college_city)
		update_college(college, entry)

def update_college(college, entry):
	college.graduate_earnings = entry["latest.earnings.10_yrs_after_entry.working_not_enrolled.mean_earnings"]
	college.median_original_aid = entry["latest_aid.loan_principal"]
	college.in_state_tuition = entry["latest.cost.tuition.in_state"]
	college.out_of_state_tuition = entry["latest.cost.tuition.out_of_state"]
	college.admission_rate = entry["latest.admissions.admission_rate.overall"]
	college.completion_rate = entry["latest.completion.completion_rate_4yr_150nt"]
	add_majors(college, entry)

def add_majors(college, entry):
	keys = major_list.split(",")
	for key in keys:
		substring = key.replace("latest.academics.program_percentage.", "")
		Major()


'''
College fields:
ID
Name
City
Undergrad Size
Majors
In State Tuition
Out of State Tuition
Grad Earnings
Median principle financial aid
admission rate
completion rate

'''

# def add_college(results):
# 	for result in results:
# 		college = College(name=result["school.name"], )

#pages = ["&page=%d" % x for x in range(data["metadata"]["total"] // 100)]
#for page in pages:
	#print(request + page)
	#print(json.dumps(requests.get(request + page).json(), indent=4))


#test2 = requests.get("https://www.numbeo.com/api/city_prices?api_key=qso12dvfirx1ii&query=Austin").json()


# for x in test2["prices"]:
# 	print(x["item_name"])


