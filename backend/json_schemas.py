from flask_marshmallow import Marshmallow, fields
from models import *

ma = Marshmallow()

class WeatherEntrySchema(ma.ModelSchema):
	class Meta:
		model = WeatherEntry

class WeatherSchema(ma.ModelSchema):
	weather_entries = ma.Nested(WeatherEntrySchema, many=True)
	class Meta:
		model = Weather

class MajorSchema(ma.ModelSchema):
	class Meta:
		model = Major

class CollegeSchema(ma.ModelSchema):
	majors = ma.Nested(MajorSchema, many=True, exclude=('college',))
	class Meta:
		model = College

class EventSchema(ma.ModelSchema):
	colleges = ma.Nested(CollegeSchema, many=True)
	class Meta:
		model = Event

class CostSchema(ma.ModelSchema):
	class Meta:
		model = Cost

class EventSchema(ma.ModelSchema):
	class Meta:
		model = Event

class CitySchema(ma.ModelSchema):
	costs = ma.Nested(CostSchema, many=True, exclude=('city',))
	weather = ma.Nested(WeatherSchema, many=True)
	class Meta:
		model = City
