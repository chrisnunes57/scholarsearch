import json
import requests
#import __init__
from db_client import db
from models import *
from sqlalchemy.exc import DataError
from simplejson.errors import JSONDecodeError

def parsedata(data):
  event_list = [];
  events_to_parse = None;
  try:
    events_to_parse = data["_embedded"]["events"]
  except KeyError:
    return event_list
  for event in events_to_parse:
    e = Event()
    try:
      e.name = event["name"]
    except KeyError:
      pass
    try:
      e.api_id = event["id"]
    except KeyError:
      pass
    try:
      e.event_type = event["classifications"][0]["segment"]["name"]
    except KeyError:
      pass
    try:
      e.start_date = event["dates"]["start"]["localDate"]
    except KeyError:
      pass
    try:
      e.start_time = event["dates"]["start"]["localTime"]
    except KeyError:
      pass
    try:
      e.end_date = event["dates"]["end"]["localDate"]
    except KeyError:
      pass
    try:
      e.end_time = event["dates"]["end"]["localTime"]
    except KeyError:
      pass
    try:
      e.longitude = event["_embedded"]["venues"][0]["location"]["longitude"]
    except KeyError:
      pass
    try:
      e.latitude = event["_embedded"]["venues"][0]["location"]["latitude"]
    except KeyError:
      pass
    try:
      e.venue = event["_embedded"]["venues"][0]["name"]
    except KeyError:
      pass
    try:
      e.address = event["_embedded"]["venues"][0]["address"]["line1"] + ", " + event["_embedded"]["venues"][0]["city"]["name"] + ", " + event["_embedded"]["venues"][0]["state"]["stateCode"] + " " + event["_embedded"]["venues"][0]["postalCode"]
    except KeyError:
      pass
    try:
      e.description = event["description"]
    except KeyError:
      pass
    try:
      e.cost = event["priceRanges"][0]["max"]
    except KeyError:
      pass
    try:
      e.organizer = event["promoter"]["name"]
    except KeyError:
      pass
    try:
      e.age_restriction = event["_embedded"]["venues"][0]["generalInfo"]["childRule"]
    except KeyError:
      pass
    try:
      e.image_url = event["images"][0]["url"]
    except KeyError:
      pass
    #e.popularity = ?
    event_list.append(e)
  return event_list


# #source: https://stackoverflow.com/questions/42686300/how-to-check-if-coordinate-inside-certain-area-python
# def haversine(lon1, lat1, lon2, lat2):
#     """
#     Calculate the great circle distance between two points 
#     on the earth (specified in decimal degrees)
#     """
#     # convert decimal degrees to radians 
#     lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

#     # haversine formula 
#     dlon = lon2 - lon1 
#     dlat = lat2 - lat1 
#     a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
#     c = 2 * asin(sqrt(a)) 
#     r = 6371 # Radius of earth in kilometers. Use 3956 for miles
#     return c * r

api_key = "&apikey=eyuZSTgnjGjksU0kbu9x7t8DtAFQYFu3"
city_field = "city="
api_url = "https://app.ticketmaster.com/discovery/v2/events.json?"

cities = City.query.all()
failed_cities = list()

i = 1
n = str(len(cities))
for city in cities:
  print(city.name + " (" + str(i) + "/" + n + ")")
  i += 1
  request_url = api_url + city_field + city.name + api_key
  data = None;
  try:
    data = requests.get(request_url).json()
  except JSONDecodeError:
    data = dict();
  #print(data)
  event_list = parsedata(data)
  #print(event_list)
  print(len(event_list))
  try:
    for event in event_list:
      cur_event = Event.query.filter_by(api_id = event.api_id).first()
      #print(cur_event)
      if cur_event == None:
        cur_event = event
      db.session.add(cur_event)
      city.events.append(cur_event);
      db.session.add(city)
      for college in city.colleges:
        if cur_event not in college.events:
          college.events.append(cur_event)
          db.session.add(college)
    db.session.commit()
  except DataError as e:
      failed_cities.append(city.name)
      print(e)
      db.session.rollback()
print(failed_cities)
