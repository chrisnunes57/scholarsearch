class BaseConfig(object):
    DEBUG = False
    TESTING = False


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    # SQLALCHEMY_DATABASE_URI = "mysql://root:scholarsearchphase2@127.0.0.1/scholarsearchdb"


class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "mysql://root:scholarsearchphase2@127.0.0.1/scholarsearchdb"


class DeploymentConfig(BaseConfig):
    DEBUG = False
    TESTING = False
