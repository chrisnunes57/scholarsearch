# app.py

# Required imports
import os
from flask import Flask
from db_client import db
from json_schemas import ma
import config
import pymysql
from flask_cors import CORS

pymysql.install_as_MySQLdb()

# Initialize Flask app
app = Flask(__name__)
CORS(app)

# #for testing;
#app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root:scholarsearchphase2@127.0.0.1/scholarsearchdb"

app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql+pymysql://root:scholarsearchphase2@/scholarsearchdb?unix_socket=/cloudsql/scholarsearch-254800:us-central1:scholarsearch-phase2-db1"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

db.init_app(app)
ma.init_app(app)

from routes import routes
app.register_blueprint(routes)

config = {
    "development": "config.DevelopmentConfig",
    "testing": "config.TestingConfig",
    "default": "config.DevelopmentConfig",
}

def configure_app(app):

    config_name = os.getenv("FLASK_CONFIGURATION", "default")
    app.config.from_object(config[config_name])  # object-based default configuration
    #app.config.from_pyfile("config.cfg", silent=True)  # instance-folders configuration
    print(f'app config: {config[config_name]}')

configure_app(app)


port = int(os.environ.get("PORT", 8080))
if __name__ == "__main__":
    app.run(threaded=True, host="0.0.0.0", port=port)
