from db_client import db
# print(db.metadata.reflect(engine=engine))
# Cost.__table__.create(db.session.bind, checkfirst=True)

college_event_table = db.Table(
    "college_event_table",
    db.Column("college_id", db.Integer, db.ForeignKey("college.id"), primary_key=True),
    db.Column("event_id", db.Integer, db.ForeignKey("event.id"), primary_key=True),
)

city_event_table = db.Table(
    "city_event_table",
    db.Column("city_id", db.Integer, db.ForeignKey("city.id"), primary_key=True),
    db.Column("event_id", db.Integer, db.ForeignKey("event.id"), primary_key=True),
)


class College(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    api_id = db.Column(
        db.Integer, unique=True, nullable=False
    )  # for pulling from the College Scorecard API again
    name = db.Column(db.String(120), unique=True, nullable=False)
    undergrad_size = db.Column(db.Integer, nullable=False)
    majors = db.relationship("Major", backref="college", lazy="dynamic")
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=False)
    url = db.Column(db.String(120), unique=False)
    img_src = db.Column(db.String(300))
    popular_major = db.Column(db.String(120))
    in_state_tuition = db.Column(db.Numeric(9, 2, asdecimal=False))
    out_of_state_tuition = db.Column(db.Numeric(9, 2, asdecimal=False))
    graduate_earnings = db.Column(
        db.Numeric(9, 2, asdecimal=False)
    )  # mean annual earnings for students working and not enrolled 10 years after entry
    median_original_aid = db.Column(
        db.Numeric(9, 2, asdecimal=False)
    )  # median principal amount of financial aid
    admission_rate = db.Column(db.Numeric(6, 5, asdecimal=False))
    completion_rate = db.Column(db.Numeric(6, 5, asdecimal=False))
    events = db.relationship("Event", secondary=college_event_table)

    def __repr__(self):
        return "<College %r>" % self.name

class Weather(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), unique=True, nullable=False)
    weather_entries = db.relationship("WeatherEntry", backref="weather", lazy="dynamic")

    def __repr__(self):
        return "<Weather %r>" % self.id

class WeatherEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    temp = db.Column(db.Numeric(5, 2, asdecimal=False), nullable=False)
    temp_min = db.Column(db.Numeric(5, 2, asdecimal=False), nullable=False)
    temp_max = db.Column(db.Numeric(5, 2, asdecimal=False), nullable=False)
    humidity = db.Column(db.Integer, nullable=False)
    state = db.Column(db.String(50), nullable=False)
    date = db.Column(db.String(10), nullable=False)
    time = db.Column(db.String(10), nullable=False)
    weather_id = db.Column(db.Integer, db.ForeignKey("weather.id"), nullable=False)

    icon_url = db.Column(db.String(300), nullable=False)


    def __repr__(self):
        return "<WeatherEntry %r>" % self.id


class Major(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    percent_of_undergrad = db.Column(db.Numeric(6, 5, asdecimal=False), nullable=False)
    college_id = db.Column(db.Integer, db.ForeignKey("college.id"), nullable=False)

    def __repr__(self):
        return "<Major %r>" % self.name


class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True, nullable=False)
    state = db.Column(db.String(2), nullable=False)
    img_src = db.Column(db.String(300))
    population = db.Column(db.Integer)
    avg_monthly_salary = db.Column(db.Numeric(12, 2, asdecimal=False))
    monthly_cost_of_living = db.Column(db.Numeric(12, 2, asdecimal=False))
    weather = db.relationship("Weather", backref="city", lazy="dynamic")
    longitude = db.Column(db.Numeric(14,10, asdecimal=False))
    latitude = db.Column(db.Numeric(14,10, asdecimal=False))
    colleges = db.relationship("College", backref="city", lazy="dynamic")
    costs = db.relationship("Cost", backref="city", lazy="dynamic")
    events = db.relationship("Event", secondary=city_event_table)
    weather = db.relationship("Weather", backref="city", lazy="dynamic")

    def __repr__(self):
        return "<City %r>" % self.name

class Cost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    category = db.Column(db.String(60), nullable=False) #used to separate diff cost categories"
    amount = db.Column(db.Numeric(12, 2, asdecimal=False), nullable=False)
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=False)

    def __repr__(self):
        return "<Cost %r>" % self.name

#Event.__table__.create(db.session.bind)

class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    api_id = db.Column(db.String(100))
    event_type = db.Column(db.String(100))

    
    start_date = db.Column(db.String(100))
    start_time = db.Column(db.String(100))
    end_date = db.Column(db.String(100))
    end_time = db.Column(db.String(100))

    longitude = db.Column(db.Numeric(14,10, asdecimal=False))
    latitude = db.Column(db.Numeric(14,10, asdecimal=False))
    venue = db.Column(db.String(100))
    address = db.Column(db.String(100))

    description = db.Column(db.String(1000))
    image_url = db.Column(db.String(300))
    
    cost = db.Column(db.Numeric(9, 2, asdecimal=False))
    organizer = db.Column(db.String(100))
    
    age_restriction = db.Column(db.String(1000))

    colleges = db.relationship("College", secondary=college_event_table)
    cities = db.relationship("City", secondary=city_event_table)

    def __repr__(self):
        return "<Event %r>" % self.name



# Colleges
# Population by major
#  Graduate earnings
#  Most popular major per college
#  Financial aid received
#  Cost of tuition

# Cities
#  Population
#  Cost of Living
#  Weather
#  Commuting options/transportation
#  Jobs in city
